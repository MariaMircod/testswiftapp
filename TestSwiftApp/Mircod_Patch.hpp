#pragma once
#include "Mircod_ECG.hpp"
#include "Mircod_PPG.hpp"

class Mircod_Patch_class
{
public:
    ECG_class ECG_signal_class;
	PPG_class PPG_signal_class;
	Mircod_Patch_class();
};
