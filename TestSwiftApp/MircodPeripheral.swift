import Foundation
import CoreBluetooth

@objc public protocol MircodPeripheralDelegate : NSObjectProtocol {
    /**
     * @discussion Invoked when all the characteristics from one service will be proccessed.
     */
    @available(*, deprecated, message: "No longer available. For follow to sensor list updating use method newSensor")
    func listOfSensorsIsLoadedFromPeripheral(_ peripheral : MircodPeripheral)
    /**
     * @discussion Implement this method to  receive data proccesing
     * @param peripheral  is instance MircodPeripheral
     * @param sensor is instance of Sensor whose value has been retrieved.
     */
    func newSensor(_ sensor : MircodSensor, peripheral : MircodPeripheral)
    /**
     * @discussion Implement this method to  receive data proccesing
     * @param peripheral  is instance MircodPeripheral
     * @param value is instance of NSNumber
     * @param sensor is instance of Sensor whose value has been retrieved.
     */
    func newValueFromPeripheral(_ peripheral : MircodPeripheral ,value: Double , sensor : MircodSensor)
    /**
     * @discussion Implement this method to  receive data with date proccesing.
     * @param peripheral  is instance MircodPeripheral
     * @param values is instance of array double values
     * @param date is instance of Date
     * @param sensor is instance of Sensor whose value has been retrieved.
     * @param progress is count values that were not sent yet.
     */
    func newDateValueFromPeripheral(_ peripheral : MircodPeripheral, values: [Double] , sensor : MircodSensor , date : Date , progress : Double)
    
    
    @available(*, unavailable, message: "no longer available ...")
    func newDateValueFromPeripheral(_ peripheral : MircodPeripheral, value: Double , sensor : MircodSensor , date : Date , progress : Double)
    
    /**
     * @discussion Implement this method to be aware of where timer time at connection  has expired
     * peripheral  is instance MircodPeripheral
     * @param peripheral is instance of MircodPeripheral which not connected
     */
    @objc optional  func timeOfTheTimerLeftFromPeripheral(_ peripheral : MircodPeripheral)
    /**
     * @discussion Invoked when you discover the characteristics of a specified service.
     * @param peripheral  is instance MircodPeripheral
     * @param characteristic is instance of CBCharacteristic which found with the selected service
     */
    @objc optional func newCharacteristicFromPeripheral(_ peripheral : MircodPeripheral , characteristic : CBCharacteristic)
    
    
    /**
     * @discussion Implement this method to receive data
     * @param  peripheral  is instance MircodPeripheral
     * @param bytes instance of NSData is a set of bytes
     * @param characteristic instance of CBCharacteristic whose value has been retrieved.
     */
    @objc optional  func newBytesFromPeripheral(_ peripheral : MircodPeripheral , bytes : NSData , fromCharacteristic: CBCharacteristic)
    
    /*!
     *  @param peripheral  is instance MircodPeripheral
     *  @param RSSInumber    The current RSSI of the link.
     *  @param error        If an error occurred, the cause of the failure.
     */
    @objc optional func didReadRSSIFromPeripheral(_ peripheral : MircodPeripheral , RSSInumber : NSNumber , error:Error?)
    
    
    /*!
     *  @param peripheral  is instance MircodPeripheral
     *  @param sensor    is instance of MircodSensor
     *  @param error     If an error occurred, the cause of the failure.
     */
    @objc optional func didWriteValueFor(_ peripheral: MircodPeripheral,  sensor: MircodSensor, error: Error?)
    
    /*!
     *  @param peripheral  is instance MircodPeripheral
     *  @param battery  The current battery status.
     */
    @objc optional func didUpdateBatteryStateFromPeripheral(_ peripheral : MircodPeripheral , battery : BatteryStatus)
    /*!
     *  @param peripheral  is instance MircodPeripheral
     *  @param power  The current power status.
     */
    @objc optional func didUpdatePowerStateFromPeripheral(_ peripheral : MircodPeripheral , power : PowerStatus)
    /*!
     *  @param peripheral  is instance MircodPeripheral
     *  @param recording  The current recording status.
     */
    @objc optional func didUpdateRecordingStateFromPeripheral(_ peripheral : MircodPeripheral , recording : RecordingStatus)
}



/*!
 *  @class MircodPeripheral
 *
 *  @discussion Represents class from mircod company for work with peripheral
 */
open class MircodPeripheral : NSObject {
    
    private var hiddenDelegate: PrivateMircodPeripheral?
    /*!
     *  @property mircodDelegate
     *  @discussion The delegate object that will receive central events like resieve data, observe and disobserve to characteristic.
     *
     */
    open var delegate: MircodPeripheralDelegate?{
        get {
            return self.hiddenDelegate?.delegate ?? nil
        }
        set{
            self.hiddenDelegate?.delegate = newValue
        }
    }
    /*!
     *  @property name
     *  @discussion The name of the peripheral.
     */
    open  var  name : String{
        get {
            return self.hiddenDelegate?.name ?? ""
        }
    }
    /*!
     *  @property macadress
     *  @discussion The macadress of the peripheral.
     */
    open  var  macadress : String{
        get {
            return self.hiddenDelegate?.macadress ?? ""
        }
    }
    /*!
     *  @property state
     *  @discussion The state of the device (main or bootloader).
     */
    open  var  state : DeviceState{
        get {
            return (self.hiddenDelegate?.state)!
        }
    }
    /*!
     *  @property isConnected is bool type
     *  @discussion if sensor is connected return true
     *
     */
    open  var isConnected : Bool{
        set{
            self.hiddenDelegate?.isConnected = newValue
        }
        get {
            return self.hiddenDelegate?.isConnected ?? false
        }
    }
    
    /*!
     *  @property isAvailable is bool type
     *  @discussion if sensor is available for connect return true
     *
     */
    open  var isAvailable : Bool {
        
        get {
            return (self.hiddenDelegate?.isAvailable)!
        }
    }
    
    /*!
     *  @property identifier is string format
     *
     *  @discussion The unique, persistent identifier associated with the peer.
     */
    open  var identifier : String{
        get {
            return self.hiddenDelegate?.peripheral?.identifier.uuidString ?? ""
        }
    }
    /*!
     *  @property firmwareVersion is string format
     *
     *  @discussion The version firmware.
     */
    open  var firmwareVersion : String?{
        get {
            return self.hiddenDelegate?.firmwareVersion
        }
    }
    /*!
     *  @property characteristics instans of NSMutableArray
     *
     *  @discussion Mutable array  of the CBCharacteristic after scanning peripheral's service.
     *
     */
    open  var characteristics : [CBCharacteristic]{
        get {
            return (self.hiddenDelegate?.characteristics)!
        }
    }
    
    @available(* , unavailable , renamed: "deviceType")
    public  var type : DeviceType!{
        get {
            return (self.hiddenDelegate?.deviceType)!
        }
    }
    /*!
     *  @property deviceType
     *
     *  @discussion The type of the device.
     */
    open  var deviceType : DeviceType!{
        get {
            return (self.hiddenDelegate?.deviceType)!
        }
    }
    /*!
     *  @property basicSensors instans of array
     *
     *  @discussion The array of MircodSensor
     */
    open  var basicSensors : [MircodSensor]{
        get {
            return (self.hiddenDelegate?.basicSensors)!
        }
    }
    /*!
     *  @property sensorsAfterAlgorithmsWork instans of array
     *
     *  @discussion The array of MircodSensor found after processing of all characteristics
     */
    open  var sensorsAfterAlgorithmsWork : [MircodSensor]{
        get {
            return (self.hiddenDelegate?.sensorsAfterAlgorithmsWork)!
        }
    }
    /*!
     *  @property selectedSensorsForObserve instans of array
     *
     *  @discussion The array of selected MircodSensor for notifying
     */
    open  var selectedSensorsForObserve : [String : [MircodSensor]]{
        get {
            return (self.hiddenDelegate?.selectedSensorsForObserve)!
        }
    }
    /*!
     *  @property is self scanner
     *
     */
    open var scanner : MircodScanner?{
        get {
            return self.hiddenDelegate?.scanner ?? nil
        }
        set{
            self.hiddenDelegate?.scanner = newValue
        }
    }
    
    /*!
     *  @property of instance CBPeripheral for connection
     *
     */
    open var peripheral : CBPeripheral?{
        get {
            return self.hiddenDelegate?.peripheral ?? nil
        }
        set{
            self.hiddenDelegate?.peripheral = newValue
        }
    }
    
    public override init() {
        super.init()
        hiddenDelegate = PrivateMircodPeripheral.init()
    }
    
    /*!
     *
     *  @param mircodDelegate The delegate that will receive central role events.
     *
     */
    public  convenience init(delegate : MircodPeripheralDelegate) {
        self.init()
        hiddenDelegate = PrivateMircodPeripheral.init(delegate: delegate, outer: self)
    }
    /*!
     *
     *  @param peripheral of instance CBPeripheral for connection.
     *
     */
    public convenience init (peripheral : CBPeripheral){
        self.init()
        hiddenDelegate = PrivateMircodPeripheral.init(peripheral: peripheral, outer: self)
        
    }
    
    /*!
     *
     *  @param peripheral of instance CBPeripheral for connection.
     *  @param mircodDelegate The delegate that will receive central role events.
     *
     */
    public convenience init(peripheral : CBPeripheral ,delegate : MircodPeripheralDelegate) {
        self.init()
        hiddenDelegate = PrivateMircodPeripheral.init(peripheral: peripheral, delegate:delegate, outer: self)
    }
    
    /*!
     *
     *  @param peripheral of instance CBPeripheral for connection.
     *  @param advertisementData is dictionary containing any advertisement and scan response data.
     *
     */
    public convenience init(peripheral:CBPeripheral, advertisementData:[String : Any]){
        self.init()
        hiddenDelegate = PrivateMircodPeripheral.init(peripheral: peripheral, advertisementData: advertisementData, outer: self)
    }
    /*!
     * @discussion Reboot device from main to bootloader
     */
    open func reboot(){
        hiddenDelegate?.reboot()
    }
    /*!
     * @discussion activate comand
     */
    open func activateComand(_ comand : ActionComand){
        hiddenDelegate?.activateComand(comand)
    }
    /**
     Write data to sensor.
     
     - Important:
     before check MircodSensor.isWritable
     
     - parameters:
     - sensor: is instance MircodSensor
     - value: is instance Data
     
     */
    open func writeToSensor(sensor : MircodSensor , value : Data){
        hiddenDelegate?.writeToSensor(sensor : sensor , value : value)
    }
    
    /*!
     * @discussion Observe selected sensor
     * @param sensor: of instance SensorName
     */
    open func observeSensor(sensor : MircodSensor){
        hiddenDelegate?.observeSensor(sensor: sensor)
    }
    
    /**
     * @discussion Observe all sensors
     */
    open func observeAllSensors(){
        hiddenDelegate?.observeAllSensors()
    }
    
    /**
     * @discussion Disobserve selected sensor
     * @param characteristic of instance CBCharacteristic
     */
    open func disobserveSensor(sensor : MircodSensor){
        hiddenDelegate?.disobserveSensor(sensor: sensor)
    }
    
    /**
     * @discussion Remove all sensors and characteristics
     */
    open func clean(){
        hiddenDelegate?.clean()
    }
    
    /**
     * @discussion Disobserve selected sensor
     * @param mac of instance String
     * @param name of instance String
     * @return mock object MircodPeripheral
     */
    public static func getMockObject(withMacadress mac: String , name : String?) -> MircodPeripheral {
        let mircodPeripheral  = MircodPeripheral.init()
        mircodPeripheral.hiddenDelegate?.macadress = mac
        mircodPeripheral.hiddenDelegate?.update(name: name ?? "")
        return mircodPeripheral;
    }
}


private class PrivateMircodPeripheral : NSObject, CBPeripheralDelegate{
    private weak var outer: MircodPeripheral?
    var  name : String?
    var  macadress = ""
    
    var  state : DeviceState = .ERRORState
    var identifier : String? {
        get {
            return peripheral?.identifier.uuidString
        }
    }
    var firmwareVersion : String? {
        get {
            if let data = firmwareChar?.value {
                if let string = String(data:data, encoding: .utf8) {
                    return string
                }
                else {return nil
                }
            }
            else{
                return nil
            }
        }
    }
    var isConnected = false
    var isAvailable : Bool {
        get {
            if peripheral != nil{
                return true
            }
            else {
                return false
            }
        }
    }
    var battery : BatteryStatus = .none {
        didSet{
            if battery != oldValue {
                delegate?.didUpdateBatteryStateFromPeripheral?(outer!, battery: battery)
            }
        }
    }
    var power : PowerStatus = .none{
        didSet{
            if power != oldValue {
                delegate?.didUpdatePowerStateFromPeripheral?(outer!, power: power)
            }
        }
    }
    var recording : RecordingStatus = .none{
        didSet{
            if recording != oldValue {
                delegate?.didUpdateRecordingStateFromPeripheral?(outer!, recording: recording)
            }
        }
    }
    var characteristics : [CBCharacteristic]
    var deviceType : DeviceType!
    var basicSensors : [MircodSensor]
    var sensorsAfterAlgorithmsWork : [MircodSensor]
    var peripheral : CBPeripheral?
    var selectedSensorsForObserve : [String : [MircodSensor]]
    var timerAtConnection : Timer?
    var bridge : Bridge
    
    let standartServicesUUID = ["FFE0" : "Main services" ,
                                "FE59" : "Bootloader state services",
                                "180F" : "Battery service",
                                "FFD0" : "Bootloader services" ,
                                "D02A" : "HUMMER service" ,
                                "D01A": "BRUSH service" ,
                                "D03A" : "Temp patch service",
                                "180A" : "Device info",
                                "D2970001-FFE3-4A85-BC6D-4A7B24C5D054" : "INSUPEN service" ,
                                "A6BA0001-1B06-49C1-9B5A-58E937AE3094" : "MIRCOD service" ,
                                "1773D05A-AC59-11E8-9A57-8EAE33A81C82" : "CardioStudy" ]
    
    var scanner : MircodScanner?
    var bootloader: CBCharacteristic?
    var action : CBCharacteristic?
    private var firmwareChar : CBCharacteristic?
    private var hardwareChar : CBCharacteristic?
    private var manafactureChar : CBCharacteristic?
    open var delegate: MircodPeripheralDelegate?
    
    override init() {
        selectedSensorsForObserve = [String : [MircodSensor]]()
        bridge = Bridge.init()
        deviceType = .ERRORType
        state = .ERRORState
        basicSensors = [MircodSensor]()
        sensorsAfterAlgorithmsWork = [MircodSensor]()
        characteristics = [CBCharacteristic]()
        name = peripheral?.name
        super.init()
    }
    
    private convenience init(outer: MircodPeripheral) {
        self.init()
        self.outer = outer
        
    }
    
    public  convenience init(delegate : MircodPeripheralDelegate,  outer : MircodPeripheral) {
        self.init(outer:outer )
        self.delegate = delegate
        name = peripheral?.name
    }
    
    public convenience init (peripheral : CBPeripheral ,outer : MircodPeripheral){
        self.init(outer:outer )
        self.peripheral = peripheral
        name = peripheral.name
    }
    
    public convenience init(peripheral : CBPeripheral ,delegate : MircodPeripheralDelegate, outer : MircodPeripheral) {
        self.init(outer:outer )
        self.delegate = delegate
        self.peripheral = peripheral
        peripheral.delegate = self
        name = peripheral.name
    }
    
    public convenience init(peripheral:CBPeripheral, advertisementData:[String : Any] , outer : MircodPeripheral){
        self.init(outer: outer)
        self.peripheral = peripheral;
        self.peripheral?.delegate = self;
        name = advertisementData[CBAdvertisementDataLocalNameKey] as? String
        var color = TempPatchColor.ERRORColor
        
        //check macadress
        if let manufacturerData = advertisementData["kCBAdvDataManufacturerData"] as? Data {
            macadress = getMacadress(fromData: manufacturerData)
            if manufacturerData.count == 9 {
                color = getColor(fromString: String(format : "%02X", manufacturerData[8]))
            }
        }
        
        //check type
        if let uuidKey = advertisementData[CBAdvertisementDataServiceUUIDsKey]{
            if let uuid = (uuidKey as AnyObject).firstObject {
                if uuid is CBUUID {
                    let uuidString = (uuid as! CBUUID).uuidString
                    
                    
                    if  !(uuidString.isEmpty) {
                        if uuidString.contains(BOOTLOADER_STATE_SERVICES_UUID ) == true {
                            self.state = .Bootloader
                            print("uuid " + uuidString)
                        }
                        else {
                            self.state = .Main
                        }
                        if uuidString.contains(HUMMER_UUID) == true {
                            deviceType = .Tool(type: .Hammer)
                        }
                        else if uuidString.contains(PATCH_UUID) == true {
                            deviceType = .Patch
                        }
                        else if uuidString.contains(BRUSH_UUID) == true {
                            deviceType = .Brush
                        }
                        else if uuidString.contains(TEMP_PATCH_UUID) == true  {
                            deviceType = .TempPatch(color: color)
                        }
                        else if uuidString.contains(INSUPEN_UUID) == true  {
                            deviceType = .Insupen(type: .TresibaFlexTouch)
                            print("uuid " + uuidString)
                        }
                        else if uuidString.contains(CARDIOSTUDY_UUID) == true  {
                            deviceType = .CardioStudy
                        }
                    }
                    
                }
            }
        }
    }
    
    private func getMacadress(fromData manufacturerData : Data) -> String {
        var mac = ""
        if manufacturerData.count >= 4 {
            let state = String(format: "%02X", manufacturerData[3])
            //            print("\nState - "+String(format: "%02X", state)) //->05
            macadress = state
        }
        if manufacturerData.count >= 6 {
            //Constructing 2-byte data as big endian (as shown in the Java code)
            let batteryVoltage = String(format: "%04X",UInt16(manufacturerData[4]) << 8 + UInt16(manufacturerData[5]))
            //            print("\nBatteryVoltage - "+String(format: "%04X", batteryVoltage)) //->0C6F
            mac += batteryVoltage
        }
        if manufacturerData.count >= 7 {
            //32- is the BLE packet counter.
            let packetCounter = String(format: "%02X",manufacturerData[6])
            mac += packetCounter
            //            print("\nPacketCounter - "+String(format: "%02X", packetCounter)) //->32
            
            //        if manufacturerData.count == 9 {
            //            let colorStr = String(format : "%02X", manufacturerData[8])
            //            mac += colorStr
            //        }
        }
        return mac
    }
    
    private func getColor(fromString str : String) -> TempPatchColor {
        switch str {
        case "0E":
            return  .blue
        case "0B":
            return  .red
        case "01":
            return  .blue
        default : return  .ERRORColor
        }
    }
    
    private func getInsupenType(fromString str : String) -> InsupenType {
        switch str {
        case "0E":
            return  .LantusSolostar
        case "0B":
            return  .TresibaFlexTouch
        case "01":
            return  .ToujeoSolostar
        case "02":
            return  .NovoRapidFlexPen
        case "03":
            return  .HumalogKwikpen
        default : return  .ERROR
        }
    }
    
    
    public func clean(){
        selectedSensorsForObserve.removeAll()
        sensorsAfterAlgorithmsWork.removeAll()
        characteristics.removeAll()
        basicSensors.removeAll()
    }
    
    //    @objc func timeOfTheTimerLeft(){
    //        delegate?.timeOfTheTimerLeftFromPeripheral(self)
    //    }
    
    //    func startConnection(){
    //        timeкAtConnection = Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(timeOfTheTimerLeft), userInfo: nil, repeats: false)
    //    }
    
    public func reboot(){
        if bootloader != nil {
            // Patch need any value for reboot. Hummer need to [0x02].
            let bytes = [0x02];
            let data = NSData.init(bytes: bytes, length: 1)
            peripheral?.writeValue(data as Data, for: bootloader!, type: .withResponse)
        }
        else {
            print("Not bootloader")
        }
    }
    public func start(){
        if action != nil {
            // Patch need any value for reboot. Hummer need to [0x03].
            let bytes = [0x03];
            let data = NSData.init(bytes: bytes, length: 1)
            peripheral?.writeValue(data as Data, for: action!, type: .withResponse)
        }
        else {
            print("Not action")
        }
    }
    public func stop(){
        if action != nil {
            let bytes = [0x04];
            let data = NSData.init(bytes: bytes, length: 1)
            peripheral?.writeValue(data as Data, for: action!, type: .withResponse)
        }
        else {
            print("Not action")
        }
    }
    public func startRecord(){
        if action != nil {
            let bytes = [0x05];
            let data = NSData.init(bytes: bytes, length: 1)
            peripheral?.writeValue(data as Data, for: action!, type: .withResponse)
        }
        else {
            print("Not action")
        }
    }
    public func stopRecord(){
        if action != nil {
            // Patch need any value for reboot. Hummer need to [0x06].
            let bytes = [0x06];
            let data = NSData.init(bytes: bytes, length: 1)
            peripheral?.writeValue(data as Data, for: action!, type: .withResponse)
        }
        else {
            print("Not action")
        }
    }
    
    public func activateComand(_ comand : ActionComand){
        switch comand {
        case .startRecord:
            startRecord()
        case .stopRecord:
            stopRecord()
        case .updateTime:
            updateTime()
        case .bootlooder:
            reboot()
        case .start:
            start()
        case .stop:
            stop()
        }
    }
    
    
    public func updateTime(){
        if action != nil {
            let value = Date().timeIntervalSince1970 + Double(TimeZone.current.secondsFromGMT())
            
            //Create UInt32 bytes
            let interval1 = UInt32.init(value/(256.0*256.0*256.0))
            let interval2 = UInt32.init(value/(256.0*256.0))
            let interval3 = UInt32.init(value/256.0)
            let interval4 = UInt32.init(value) - interval1 - interval2 - interval3
            
            //Start bytes
            var bytesStart : [UInt8] =  [0x00]
            bytesStart.append(0x04)
            let dataStart = Data.init(bytes: bytesStart)
            
            //Date value bytes
            var data = Data.init(bytes: [interval4], count: 1)
            data += Data.init(bytes: [interval3], count: 1)
            data += Data.init(bytes: [interval2], count: 1)
            data += Data.init(bytes: [interval1], count: 1)
            let result = dataStart  + data
            
            peripheral?.writeValue(result, for: action!, type: .withResponse)
        }
        else {
            print("Not action")
        }
    }
    
    
    public func writeToSensor(sensor : MircodSensor , value : Data){
        if sensor.isWritable == true {
            let type : CBCharacteristicWriteType = (peripheral?.canSendWriteWithoutResponse)! ? .withoutResponse : .withResponse
            
            peripheral?.writeValue(value, for: sensor.characteristic!, type: type)
        }
        else {
            print("Sensor is not writable");
        }
    }
    
    
    //Not explicit setter
    public func update(name newName : String){
        name = newName
    }
    
    public func peripheralDidUpdateName(_ peripheral: CBPeripheral) {
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]){
    }
    
    public func peripheralDidUpdateRSSI(_ peripheral: CBPeripheral, error: Error?){
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?){
        delegate?.didReadRSSIFromPeripheral!(outer!, RSSInumber: RSSI, error: error)
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?){
        print("Count services ", (peripheral.services?.count) ?? "nil")
        for service in peripheral.services! {
            print("Discovered service:", service.uuid)
            if standartServicesUUID[service.uuid.uuidString] != nil {
                if service.uuid.uuidString == BOOTLOADER_STATE_SERVICES_UUID{
                    state = .Bootloader
                }
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
    }
    
    
    public func observeAllSensors(){
        for sensor in sensorsAfterAlgorithmsWork{
            observeSensor(sensor: sensor)
        }
    }
    
    public func observeSensor(sensor : MircodSensor){
        peripheral?.setNotifyValue(true, for: sensor.characteristic!)
        sensor.isObserve = true
        let uuid = sensor.uuid.rawValue
        var array : [MircodSensor] = selectedSensorsForObserve[uuid] ?? [MircodSensor]()
        array.append(sensor)
        selectedSensorsForObserve.updateValue(array, forKey: uuid)
    }
    
    public func disobserveSensor(sensor : MircodSensor){
        let uuid = sensor.uuid.rawValue
        var array : [MircodSensor] = selectedSensorsForObserve[uuid]!
        array.remove(object: sensor)
        selectedSensorsForObserve.updateValue(array, forKey: uuid)
        sensor.isObserve = false
        var observeChar = false
        for sensorEnum in  array {
            if sensor.characteristic == sensorEnum.characteristic {
                if sensorEnum.isObserve{
                    observeChar = true
                    break
                }
            }
        }
        if  !observeChar {
            peripheral?.setNotifyValue(observeChar, for: sensor.characteristic!)
        }
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?){
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?){
        
        if service.uuid.uuidString == BOOTLOADER_SERVICES_UUID {
            bootloader = service.characteristics?.last
        }
        else if service.uuid.uuidString == DEVICE_INFORMATION_SERVICES_UUID  {
            for achar in service.characteristics! {
                
                switch achar.uuid.uuidString{
                //firmware
                case "2A26":
                    firmwareChar = achar
                    
                //manafacture
                case "2A29":
                    manafactureChar = achar
                    
                // hardware
                case "2A27":
                    hardwareChar = achar
                default: continue
                }
            }
        }
        else {
            
            for characteristic in service.characteristics!{
                peripheral.discoverDescriptors(for: characteristic)
                characteristics.append(characteristic)
                print(characteristic.uuid.uuidString)
            }
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?){
        if characteristic.uuid.uuidString == STATUS_UUID{
            peripheral.readValue(for: characteristic)
            if let data = characteristic.value{
                power = PowerStatus.init(rawValue:UInt16(data[0])) ?? .none
                recording = RecordingStatus.init(rawValue:UInt16(data[1])) ?? .none
                battery = BatteryStatus.init(rawValue: UInt16(data[2])) ?? .none
            }
        }
        else {
            proccessDataFromCharacteristic(characteristic)
        }
    }
    
    private var countECG = 0
    
    func proccessDataFromCharacteristic(_ characteristic : CBCharacteristic!)  {
        
        var sensor : MircodSensor!
        if let descriptors = characteristic.descriptors {
            sensor = MircodSensor.init(descriptors: descriptors)
        }
        if sensor.name == .ERROR {
            sensor = MircodSensor.init(cbuuid: characteristic.uuid)
        }
        if let data = characteristic.value{
            
            switch sensor.uuid {
                
            case .sensorTIME_TEMP :
                let temp = Double(Int16(data[0]) << 8 + Int16(data[1]))
                let progress = Double(Int16(data[6]) << 8 + Int16(data[7]))
                
                var interval = Int(UInt32(data[2]))*256*256*256
                interval += Int(UInt32(data[3]))*256*256
                interval += Int(UInt32(data[4]))*256
                interval += Int(UInt32(data[5]))
                let date = Date.init(timeIntervalSince1970:TimeInterval(interval))
                
                delegate?.newDateValueFromPeripheral(outer! ,values : [temp/100.0] , sensor : sensor , date : date, progress: progress )
                
            case .sensorYAWPITCHROLL , .sensorACCLERATION :
                
                let dic : [String : Int]  = sensor.uuid == .sensorACCLERATION ?
                    bridge.proccessAccelerationData(data) as!  [String : Int] :
                    bridge.proccessYawPitchRoll(data) as!  [String : Int]
                if let s = selectedSensorsForObserve[sensor.uuid.rawValue] {
                    for sensorEnum in s{
                        let val = sensor.uuid == .sensorACCLERATION ? Double(dic[sensorEnum.name.rawValue]!)/1000.0 : Double(dic[sensorEnum.name.rawValue]!)
                        delegate?.newValueFromPeripheral(outer! ,value: val, sensor: sensorEnum)
                    }
                }
            case .sensorBATTERY :
                let val = Int(Int16(data[0]))
                print("battery " + String(val))
                var count:UInt8 = 0
                characteristic.value!.copyBytes( to: &count,count: characteristic.value!.count )
                delegate?.newValueFromPeripheral(outer! ,value:Double(count) , sensor: sensor)
            case .sensorHITS :
                let value = Double(UInt32(data[0])*256*256*256 + UInt32(data[1])*256*256 + UInt32(data[2])*256 + UInt32(data[3]))
                delegate?.newValueFromPeripheral(outer! ,value: value, sensor: sensor)
                
            case .sensorTEMPERATUREAIR, .sensorTEMPERATUREBODY, .sensorHPA,.sensorLeadOFF:
                
                var value = Double(UInt32(data[0])*256 + UInt32(data[1]))
                
                if sensor.uuid == .sensorTEMPERATUREBODY || sensor.uuid == .sensorTEMPERATUREAIR{
                    value/=10.0
                }
                delegate?.newValueFromPeripheral(outer! ,value: value, sensor: sensor)
            case .sensorECG:
                for i in 0 ... (data.count-1)/2 {
                    
                    let value = Int(Int16(data[i*2]) << 8 + Int16(data[i*2+1]))
                    countECG += 1
                    print("\(Date())" + " count \(countECG) " + "\(value)")
                    let dic  = bridge.updateECG(Int32(value))
                    if let s = selectedSensorsForObserve[sensor.uuid.rawValue] {
                        for sensorEnum in s{
                            let val =  dic?[sensorEnum.name.rawValue]
                            delegate?.newValueFromPeripheral(outer! ,value:Double(truncating: val!) , sensor: sensorEnum)
                        }
                    }
                }
            case .sensorSPO2 :
                let arrayOfValues = bridge.proccessData(data) as! [Int]
                
                for i in 0..<(arrayOfValues.count) where i%2 == 0  {
                    let dic  = bridge.updatePPGRed(Double(arrayOfValues[i]), ir: Double(arrayOfValues[i+1]))
                    for sensorEnum in selectedSensorsForObserve[sensor.uuid.rawValue]!{
                        let i = dic?[sensorEnum.name.rawValue]
                        delegate?.newValueFromPeripheral(outer!,value:Double(truncating: i!) , sensor: sensorEnum)
                    }
                    
                }
            case .sensorINSULIN :
                
                var interval = Int(UInt32(data[3]))*256*256*256
                interval += Int(UInt32(data[2]))*256*256
                interval += Int(UInt32(data[1]))*256
                interval += Int(UInt32(data[0]))
                let date = Date.init(timeIntervalSince1970:TimeInterval(interval))
                
                let doze = Int(UInt32(data[4]))
                delegate?.newDateValueFromPeripheral(outer! ,values : [Double(doze)], sensor : sensor , date : date, progress: 0 )
            case .sensorGSR:
                for i in 0..<(data.count) where i%2 == 0  {
                    let value = Double(UInt32(data[i])*256 + UInt32(data[i+1]))
                    delegate?.newValueFromPeripheral(outer!, value: value, sensor: sensor)
                }
            case .sensorMULTI_ECG :
                for i in 0..<1 {
                    var array = [Double]()
                    for j in (i*10)..<(data.count/(2-i)) where j%2 == 0  {
                        let value = Double(UInt32(data[j])*256 + UInt32(data[j+1]))
                        array.append(value)
                    }
                    delegate?.newDateValueFromPeripheral(outer!, values: array, sensor: sensor, date: Date(), progress: 100)
                }
            case .sensorSTEPS :
                var value = Int(UInt32(data[0]))*256*256*256
                value += Int(UInt32(data[1]))*256*256
                value += Int(UInt32(data[2]))*256
                value += Int(UInt32(data[3]))
                delegate?.newValueFromPeripheral(outer! ,value: Double(value), sensor: sensor)
                print(value)
            case .sensorSPEED :
                let value = Double(UInt32(data[0])*256 + UInt32(data[1]))
                delegate?.newValueFromPeripheral(outer! ,value: value/10, sensor: sensor)
                print(value)
            case .sensorMOVEMENT :
                let value = Double(UInt32(data[0]))
                delegate?.newValueFromPeripheral(outer! ,value: value, sensor: sensor)
                print(value)
            case .sensorTEMPERATURE_INSUPEN :
                let value = Double(UInt32(data[1])*256 + UInt32(data[0]))
                delegate?.newValueFromPeripheral(outer! ,value: value/10, sensor: sensor)
            case .sensorECG_PROCESS:
                let value = Double(UInt32(data[0]))
                delegate?.newValueFromPeripheral(outer! ,value: value, sensor: sensor)
            default : break
            }
        }
    }
    
    
    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?){
        let newSensor = MircodSensor.init(cbuuid: characteristic.uuid)
        for sensor in  sensorsAfterAlgorithmsWork{
            if sensor.name == newSensor.name {
                delegate?.didWriteValueFor?(outer!, sensor: sensor , error:error)
                break
            }
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?){
        let sensor = MircodSensor.init(cbuuid: characteristic.uuid)
        if let data = characteristic.value {
            
            switch sensor.uuid {
            case .sensorINSULIN :
                
                var interval = Int(UInt32(data[3]))*256*256*256
                interval += Int(UInt32(data[2]))*256*256
                interval += Int(UInt32(data[1]))*256
                interval += Int(UInt32(data[0]))
                let date = Date.init(timeIntervalSince1970:TimeInterval(interval))
                
                let doze = Int(UInt32(data[4]))
                delegate?.newDateValueFromPeripheral(outer! ,values : [Double(doze)], sensor : sensor , date : date, progress: 0 )
                
            default : break
            }
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?){
        if characteristic.uuid.uuidString == ACTION_UUID ||  characteristic.uuid.uuidString == ACTION_BULAT_UUID {
            action = characteristic
            start()
            updateTime()
        }
        else if characteristic.uuid.uuidString == STATUS_UUID {
            peripheral.setNotifyValue(true, for: characteristic)
        }
        let sensor = MircodSensor.init(characteristic: characteristic)
        
        //if the sensor is not recognized
        guard sensor.name != .ERROR else {
            return
        }
        
        for s in MircodSensor.init().getSensorsFrom(uuid: sensor.uuid){
            if (s.name != .ERROR ){
                s.characteristic = characteristic
                sensorsAfterAlgorithmsWork.append(s)
                
                peripheral.readValue(for: characteristic)
                if characteristic.value != nil {
                    proccessDataFromCharacteristic(characteristic)
                }
            }
            else {
                
            }
        }
        delegate?.newSensor(sensor, peripheral: outer!)
        delegate?.listOfSensorsIsLoadedFromPeripheral(outer!)
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: Error?){
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?){
        print("didWriteValueForCharacteristic " + String(describing: error));
    }
    
}
