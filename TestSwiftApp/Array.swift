//
//  Array.swift
//  TestSwiftApp
//
//  Created by Maria on 19.09.2018.
//  Copyright © 2018 Mircod. All rights reserved.
//

import Foundation
extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}
