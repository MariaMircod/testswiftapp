//
//  ObjcClass.h
//  MirCode
//
//  Created by Мария Тимофеева on 16.03.17.
//  Copyright © 2017 ___matim___. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BridgeClass : NSObject

-(NSDictionary<NSString*, NSNumber*>*)updateECG:(int)data;
-(NSDictionary<NSString*, NSNumber*>*)updatePPGRed:(double)Red IR:(double)IR;
@end
