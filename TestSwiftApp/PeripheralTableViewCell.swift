//
//  PeripheralTableViewCell.swift
//  TestSwiftApp
//
//  Created by Мария Тимофеева on 22.09.17.
//  Copyright © 2017 Mircod. All rights reserved.
//

import UIKit
class PeripheralTableViewCell: UITableViewCell {
    
    var  peripheral : MircodPeripheral!
    var  buttons  = [String : ConnectButton]()
    var  labels   = [String : UILabel]()
    var  updateButton : UIButton!
    var  writeButton : UIButton!
    
    func reloadLabelData(){
        for  label in labels.values.enumerated() {
            label.element.text = ""
        }
    }
    
    func initCellViews(){
        if (updateButton == nil){
            updateButton = UIButton.init(type: .system)
            updateButton.setTitle("Update firmware", for: .normal)
            updateButton.backgroundColor = .white
            updateButton.setTitleColor(.black, for: .normal)
            addSubview(updateButton)
        }
        
    }
    
    func loadView(){
        backgroundColor = UIColor.lightText
        let x = 40
        var y = 0
        let width  = Int(frame.size.width)
        let height = 50
        let widthLabel = 80
        
        backgroundColor = UIColor.clear
        for sensor in (peripheral.sensorsAfterAlgorithmsWork.enumerated()){
            let name = sensor.element.name.rawValue
            if  labels[name] == nil {
                
                let btn   =  ConnectButton.init()
                let label =  UILabel.init()
                
                btn.frame = CGRect.init(x: x, y: y, width: width, height: height)
                btn.setTitle(name, for: .normal)
                
                btn.backgroundColor = .clear;
                btn.tag = sensor.offset
                btn.contentHorizontalAlignment = .left
                btn.peripheral = peripheral
                btn.addTarget(self, action: #selector(setObserveSensor(_:)), for: .touchDown)
                label.frame = CGRect.init(x: width - widthLabel, y: y, width: widthLabel, height: height)
                label.textColor = .white
                if sensor.element.isObserve {
                    btn.setTitleColor( .white , for: .normal)
                }
                else{
                    btn.setTitleColor(.darkGray, for: .normal)
                    label.text = ""
                }
                if !buttons.keys.contains(name) {
                    buttons.updateValue(btn, forKey: name)
                    labels.updateValue(label, forKey: name)
                    
                    addSubview(btn)
                    addSubview(label)
                }
                
                if sensor.element.isWritable {
                    let writeBtn   =  WriteButton.init(type: .custom)
                    writeBtn.frame = CGRect.init(x: width-70, y: y, width: 50, height: 30)
                    writeBtn.setTitle("Write", for: .normal)
                    writeBtn.backgroundColor = .white
                    writeBtn.sensor = sensor.element
                    writeBtn.addTarget(self, action:#selector(writeToSensor(_:)) , for: .touchDown)
                    writeBtn.setTitleColor( .black, for: .normal)
                    addSubview(writeBtn)
                }
                
              
                
            }
            else {
                labels[name]?.frame = CGRect.init(x: width - widthLabel, y: y, width: widthLabel, height: height)
            }
            y+=50
        }
        updateButton.frame = CGRect.init(x : Int(width/2-60),y : peripheral.sensorsAfterAlgorithmsWork.count*50, width :  120, height : 35);
    }
    
    @objc func writeToSensor(_ sender :WriteButton ){
        let value = Date().timeIntervalSince1970 + Double(TimeZone.current.secondsFromGMT())
        
        //Create UInt32 bytes
        let interval1 = UInt32.init(value/(256.0*256.0*256.0))
        let interval2 = UInt32.init(value/(256.0*256.0))
        let interval3 = UInt32.init(value/256.0)
        let interval4 = UInt32.init(value) - interval1 - interval2 - interval3
        
        //Start bytes
        let bytesStart : [UInt8] = [0x00]
        let dataStart = Data.init(bytes: bytesStart)
        
        //Date value bytes
        var data = Data.init(bytes: [interval4], count: 1)
        data += Data.init(bytes: [interval3], count: 1)
        data += Data.init(bytes: [interval2], count: 1)
        data += Data.init(bytes: [interval1], count: 1)
        let result = dataStart  + data
        
        peripheral.writeToSensor(sensor: sender.sensor!, value: result)
    }
    
    func newValue (_ value: String , fromSensor sensor: MircodSensor){
        let label =  labels[sensor.name.rawValue]
        label?.text = value
    }
    
    @IBAction func setObserveSensor(_ sender : ConnectButton){
        let sensor = peripheral.sensorsAfterAlgorithmsWork[sender.tag]
        var color : UIColor
        
        if sensor.isObserve {
            peripheral.disobserveSensor(sensor: sensor)
            color = .darkGray
            
        }
        else{
            peripheral.observeSensor(sensor: sensor)
            color = .white
        }
        labels[sensor.name.rawValue]?.textColor = color
        sender.setTitleColor(color, for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        loadView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
