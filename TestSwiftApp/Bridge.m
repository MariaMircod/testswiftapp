//
//  Bridge.m
//  TestSwiftApp
//
//  Created by Мария Тимофеева on 15.09.17.
//  Copyright © 2017 Mircod. All rights reserved.
//

#import "Bridge.h"
#import "BridgeClass.h"
@interface Bridge()
@property (strong, nonatomic) BridgeClass *cppClass;
@end
@implementation Bridge
-(instancetype)init{
    self = [super init];
    self.cppClass = [BridgeClass new];
    return  self;
}

-(NSDictionary<NSString*, NSNumber*>*)updateECG:(int)data{
    return [self.cppClass updateECG: data];
}
-(NSDictionary<NSString*, NSNumber*>*)updatePPGRed:(double)Red IR:(double)IR{
    return [self.cppClass updatePPGRed:Red IR:IR];
}
-(NSArray*)proccesTripleData:(NSData *)data{
    NSString *stringData = [NSString stringWithFormat:@"%@", data];
    return [self stringToOrintationValueArray:stringData];
    
}

-(NSDictionary<NSString *,NSNumber *> *)proccessAccelerationData:(NSData *)data{
    NSMutableDictionary<NSString*, NSNumber*> *acceleration = [NSMutableDictionary new];
    NSArray *arr = [self proccesTripleData:data];
    [acceleration setObject:[self convertHextoNegativeInt:arr[0]] forKey:@"ACCLERATION X"];
    [acceleration setObject:[self convertHextoNegativeInt:arr[1]] forKey:@"ACCLERATION Y"];
    [acceleration setObject:[self convertHextoNegativeInt:arr[2]] forKey:@"ACCLERATION Z"];
    return [acceleration copy];
}
-(NSDictionary<NSString*, NSNumber*> *)proccessYawPitchRollData:(NSData *)data{
    NSMutableDictionary<NSString*, NSNumber*> *YawPitchRoll = [NSMutableDictionary new];
    NSArray *arr = [self proccesTripleData:data];
    [YawPitchRoll setObject:[self convertHextoNegativeInt:arr[0]] forKey:@"YAW"];
    [YawPitchRoll setObject:[self convertHextoNegativeInt:arr[1]] forKey:@"PITCH"];
    [YawPitchRoll setObject:[self convertHextoNegativeInt:arr[2]] forKey:@"ROLL"];
    return [YawPitchRoll copy];
}

-(NSArray<NSNumber *>*)proccessData:(NSData *)data{
    NSString *stringData = [NSString stringWithFormat:@"%@", data];
    NSArray<NSNumber*> *arr =[self stringToValueArray:stringData];
    NSMutableArray *values = [NSMutableArray new];
    for (NSString *value in arr) [values addObject:[self convertHextoNegativeInt:value]];
    return [values copy];
}


-(NSNumber*)convertHextoNegativeInt:(NSString *)hex{
    NSScanner *scanner = [NSScanner scannerWithString:hex];
    unsigned int temp;
    [scanner scanHexInt:&temp];
    int actualInt = (short)temp;
    return [NSNumber numberWithInteger:actualInt];
}
- (NSArray*) stringToValueArray:(NSString*)str{
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"<" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@">" withString:@""];
    NSMutableArray* numbers = [[NSMutableArray alloc] init];
    for (int i = 0 ; i < str.length/4; i++)
        [numbers addObject:[str substringWithRange:NSMakeRange(i*4, 4)]];
    return numbers;
}
- (NSArray*) stringToOrintationValueArray:(NSString*)str{
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"<" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    return @[[str substringWithRange:NSMakeRange(0, 4)],[str substringWithRange:NSMakeRange(4, 4)],[str substringWithRange:NSMakeRange(8, 4)]];
}

@end


