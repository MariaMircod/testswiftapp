//
//  ViewController.swift
//  TestSwiftApp
//
//  Created by Мария Тимофеева on 12.09.17.
//  Copyright © 2017 Mircod. All rights reserved.
//

import UIKit
import CoreBluetooth
import UserNotifications
import iOSDFULibrary
class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,MircodScannerDelegate, MircodPeripheralDelegate, MircodDFUDelegate {
    var scanner : MircodScanner!
    
    var selectedPeripheral : MircodPeripheral?
    let container: UIView = UIView()
    let actInd = UIActivityIndicatorView()
    
    
    var dfu :MircodDFU! = nil
    let logLoading: UILabel = UILabel()
    let progress: UIProgressView = UIProgressView()
    let stopButton = UIButton.init(type: .system)
    let cancelUpdate = UIButton.init(type: .custom)
    
    
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    var updateTimer: Timer?
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scanner = MircodScanner.init(delegate:self)
        dfu = MircodDFU.init(withDelegate: self, scanner: scanner)
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let select = selectedPeripheral {
            scanner.cancelConnect(withPeripheral: select)
        }
    }
    
    @IBAction func refreshData(_ sender: UIBarButtonItem) {
        reloadTableData()
    }
    
    func reloadTableData(){
        scanner.stopScanDevices()
        scanner.startScanDevices(nil)
        tableView.reloadData()
    }
    
    //MARK: Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (scanner!.peripherals.count)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return (scanner!.peripherals[section].name)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let peripheral = scanner.peripherals[section]
        
        let frame = tableView.frame
        let view = UIView.init(frame: tableView.frame)
        let isSelect = selectedPeripheral == peripheral
        view.backgroundColor = isSelect ? hexStringToUIColor(hex: "F5511E") :  hexStringToUIColor(hex: "0E0E0E")
        let label = UILabel.init(frame: CGRect.init(x: 20, y: 20, width: 100, height: Int(frame.size.height)))
        label.text = peripheral.name
        label.textColor = UIColor.white
        label.sizeToFit()
        view.addSubview(label)
        
        let isConnected = selectedPeripheral?.identifier == peripheral.identifier ? true : false
        
        let btn = ConnectButton.init(type : .system)
        btn.frame  = CGRect.init(x: Int(frame.size.width-120), y: 15, width: 100, height: 30)
        btn.setTitle(isConnected ? "Disconnect" : "Connect" , for: .normal)
        btn.backgroundColor = .white
        
        btn.addTarget(self, action:isConnected ? #selector(disconnectToPeripheral) : #selector(connectToPeripheral) , for: .touchDown)
        btn.setTitleColor( .black, for: .normal)
        btn.peripheral = peripheral
        
        
        view.addSubview(btn)
        
        return view
    }
    
    @IBAction func connectToPeripheral(sender : ConnectButton){
        if sender.peripheral != nil  {
            scanner.connect(toPeripheral: sender.peripheral!, withTimeout: nil, withDelegate: self)
            initHudView(withLabels: false)
        }
        else {
            let alert = UIAlertController(title: "Error", message: "Peripheral  is not avaliable", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Refresh data", style: UIAlertAction.Style.default, handler: { action in
                self.reloadTableData()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler:nil))
            present(alert, animated: true, completion: nil)
        }
        
    }
    @IBAction func disconnectToPeripheral(){
        if let select = selectedPeripheral {
            scanner.cancelConnect(withPeripheral: select)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  CGFloat((selectedPeripheral!.sensorsAfterAlgorithmsWork.count+1)*50);
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let peripheral = self.scanner.peripherals[section]
        if peripheral.identifier == selectedPeripheral?.identifier{
            return 1
        }
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "ListCell"
        
        var cell: PeripheralTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? PeripheralTableViewCell
        
        if cell == nil {
            tableView.register(UINib(nibName: "PeripheralTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? PeripheralTableViewCell
        }
        cell?.peripheral = selectedPeripheral
        cell?.initCellViews()
        cell.updateButton.tag = indexPath.section
        cell.updateButton.addTarget(self, action: #selector(pressUpdateButton(_:)), for: .touchUpInside)
        return cell!
    }
    
    @objc func pressUpdateButton(_ button: UIButton) {
//        initHudView(withLabels: true)
        if let peripheral = selectedPeripheral {
            peripheral.activateComand(.startRecord)
//            var name = ""
//            switch  peripheral.deviceType.category {
//            case  .ToolCategory :
//                let type = peripheral.deviceType.parametr as! ToolType
//                if type == .Hammer {
//                    name = "Hammer_FW"
//                }
//            case .BrushCategory :
//                name = "Brush_FW"
//            case .PatchCategory :
//                name = "app_dfu_package"
//            case .TempPatchCategory :
//                name = "T_Patch_M_097"
//            case .InsupenCategory :
//                name = "Insupen1"
//            default:
//                break
//            }
//            if name != "" {
//                if let fileURL = Bundle.main.url(forResource: name, withExtension: "zip") {
//                    dfu.updatePeripheral(peripheral:peripheral, withArchiveURL: (fileURL as NSURL) as URL)
//                }
//            }
//            else{
//                print("девайс не опознан")
//            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //MARK: - Mircod
    func bluetoothManagerDidUpdateState(_ state: MicodScannerState) {
        
    }
    
    func newDateValueFromPeripheral(_ peripheral: MircodPeripheral, values: [Double], sensor: MircodSensor, date: Date, progress: Double) {
        
    }
    
    
    func newSensor(_ sensor: MircodSensor, peripheral: MircodPeripheral) {
        //Get section index
        //        if let  section = scanner.peripherals.index(of: peripheral){
        //Reload section
        tableView.reloadData()
        //            tableView.reloadSections(IndexSet(integer: section), with: .automatic)
        //        }
    }
    
    
    func bluetoothManagerDidUpdateState(_ state: CBManagerState) {
        print("update state")
    }
    
    func didConnectToPeripheral(_ peripheral: MircodPeripheral) {
        print("Connect to ", peripheral.name )
        
        container.removeFromSuperview()
        actInd.stopAnimating()
        selectedPeripheral = peripheral
    }
    func disconnectPeripheral(_ peripheral: MircodPeripheral, error: Error?) {
        if selectedPeripheral == peripheral {
            selectedPeripheral  = nil
        }
        if let section = scanner.peripherals.index(of: peripheral){
            tableView.reloadSections(IndexSet(integer: section), with: .automatic)
        }
        else {
            tableView.reloadData()
        }
    }
    
    func newPeripheral(_ peripheral: MircodPeripheral) {
        tableView.reloadData()
    }
    
    func newValueFromPeripheral(_ peripheral: MircodPeripheral, value: Double, sensor: MircodSensor) {
        if let section = scanner.peripherals.index(of: (selectedPeripheral)!){
            if let cell : PeripheralTableViewCell = tableView.cellForRow(at: IndexPath.init(row: 0, section:section)) as? PeripheralTableViewCell {
                cell.newValue(String(value), fromSensor: sensor)
            }
        }
    }
    
    func listOfSensorsIsLoadedFromPeripheral(_ peripheral: MircodPeripheral) {
        
    }
    
    
    //    func newDateValueFromPeripheral(_ peripheral: MircodPeripheral, value: Double, sensor: MircodSensor, date: Date, progress: Double) {
    //
    //        print(sensor.name.rawValue + " " + String(value))
    //        print(date)
    //        print("________")
    //        if let section = scanner.peripherals.index(of: (selectedPeripheral)!){
    //            if let cell : PeripheralTableViewCell = tableView.cellForRow(at: IndexPath.init(row: 0, section:section)) as? PeripheralTableViewCell {
    //                cell.newValue(String(value), fromSensor: sensor)
    //            }
    //        }
    //    }
    
    func timeOfTheTimerLeftFromPeripheral(_ peripheral: MircodPeripheral) {
        
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func didFailConnectToPeripheral(_ peripheral: MircodPeripheral, withError error: Error?) {
        
    }
    
    
    //MARK: - Update firmware view
    
    func initHudView(withLabels : Bool){
        container.frame =  (navigationController?.view?.frame)!
        container.center = view.center
        container.backgroundColor = UIColor.init(white: 0.0, alpha: 0.5)
        actInd.frame = CGRect.init(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        actInd.style = .whiteLarge
        actInd.center = CGPoint.init(x: view.frame.size.width / 2, y: view.frame.size.height / 2)
        navigationController?.view.addSubview(container)
        container.addSubview(actInd)
        actInd.startAnimating()
        
        let red  = hexStringToUIColor(hex: "F5511E")
        let frame = view.frame;
        
        let x = 40 , interval = 10 , labelHeight = 80 , labelWidth = Int(frame.size.width)-x*2 ,  buttonHeight = 30
        var y = Int(frame.size.height*3/8)+30
        
        logLoading.isHidden = !withLabels
        progress.isHidden = !withLabels
        stopButton.isHidden = !withLabels
        cancelUpdate.isHidden = !withLabels
        
        if withLabels {
            
            logLoading.frame = CGRect.init(x: x, y: Int(y-30), width:labelWidth , height: labelHeight)
            logLoading.numberOfLines = 0
            logLoading.textColor = .white
            logLoading.textAlignment = .center
            y+=labelHeight + interval
            
            progress.frame = CGRect.init(x: 40, y: y, width: labelWidth, height: 10)
            progress.progressTintColor = red
            progress.progress = 0.0
            y+=interval + Int(progress.frame.size.height)
            
            stopButton.frame =  CGRect.init(x : x*2, y: y , width : labelWidth - x*2 , height : buttonHeight)
            stopButton.addTarget(self, action: #selector(stopButtonDidClick(_:)), for: .touchUpInside)
            stopButton.setTitle("Stop", for: .normal)
            stopButton.setTitleColor(red, for: .normal)
            y+=buttonHeight+interval
            
            cancelUpdate.frame =  CGRect.init(x : x*2, y: y , width : labelWidth - x*2 , height : buttonHeight)
            cancelUpdate.addTarget(self, action: #selector(cancelUpdate(_:)), for: .touchUpInside)
            cancelUpdate.setTitle("Cancel", for: .normal)
            cancelUpdate.setTitleColor(red, for: .normal)
            y+=buttonHeight+interval
            
            container.addSubview(logLoading)
            container.addSubview(progress)
            container.addSubview(stopButton)
            container.addSubview(cancelUpdate)
        }
    }
    
    @IBAction func stopButtonDidClick(_ button: UIButton){
        if dfu.isAborted{
            button.setTitle("Stop process" , for: .normal)
            dfu.restart()
            return;
        }
        dfu.pause()
        print("Action: DFU paused")
        
        let alertView = UIAlertController(title: "Warning", message: "Are you sure you want to stop the process?", preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "Abort", style: .destructive) {
            (action) in
            print("Action: DFU aborted")
            self.dfu.abort()
        })
        alertView.addAction(UIAlertAction(title: "Cancel", style: .cancel) {
            (action) in
            print("Action: DFU resumed")
            self.dfu.resume()
        })
        present(alertView, animated: true)
    }
    
    @objc func cancelUpdate(_ button: UIButton){
        dfu.stop()
    }
    
    func destroyHudView(){
        container.removeFromSuperview()
        actInd.stopAnimating()
    }
    
    func newLogWith(level: LogLevel, message: String) {
        print("\(level.name()): \(message)")
    }
    func MircodDfuProgressDidChangeFor(part: NSInteger, totalParts: NSInteger, progress: NSInteger, currentSpeedBytesPerSecond: Double, avgSpeedBytesPerSecond: Double) {
        
        self.progress.setProgress(Float(progress)/100.0, animated: true)
        logLoading.text = String(format: "Speed: %.1f KB/s\nAverage Speed: %.1f KB/s",
                                 currentSpeedBytesPerSecond/1024, avgSpeedBytesPerSecond/1024)
    }
    
    func errorToUpdate(error: Error?) {
        container.removeFromSuperview()
        actInd.stopAnimating()
    }
    
    func dfuError(error: DFUError, WithMessage: String) {
        
        container.removeFromSuperview()
        actInd.stopAnimating()
    }
    func changeDfuState(state: DFUState) {
        
    }
    
    
    func didAbort() {
        actInd.stopAnimating()
        stopButton.setTitle("Restart" , for: .normal)
        stopButton.isEnabled = true
    }
    
    func didPause() {
        actInd.startAnimating()
    }
    
    func didResume() {
        
    }
    
    func endLoadingWithPeripheral(peripheral: MircodPeripheral) {
        print("Sensors")
        print("uuid " + peripheral.macadress)
        if peripheral.name == "InsuDfu" {
            if  peripheral.state == .Bootloader {
                let name = "Insupen2"
                if let fileURL = Bundle.main.url(forResource: name, withExtension: "zip") {
                    dfu.updatePeripheral(peripheral:peripheral, withArchiveURL: (fileURL as NSURL) as URL?)
                }
            }
        }
        else {
            destroyHudView()
        }
        
    }
    
    
    
}
