//
//  AppDelegate.swift
//  TestSwiftApp
//
//  Created by Мария Тимофеева on 12.09.17.
//  Copyright © 2017 Mircod. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        print("didFinishLaunchingWithOptions")
        //This way we ask for the authorization once the app is opened
//        let options:UNAuthorizationOptions = [.badge,.alert,.sound]
        //        UserDefaults.standard.setValue(Date(), forKey: "date")
//        UNUserNotificationCenter.current().requestAuthorization(options: options) { (granted:Bool, error:Error?) in
//            if granted{
//                print("Notification Authorized")
//            }else{
//                print("Notification Not Authorized")
//            }
//        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        print("applicationWillResignActive")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
         print("applicationDidEnterBackground")
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("applicationWillEnterForeground")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        print("applicationDidBecomeActive")
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
      BackgroundBLEManager.instance.start()
//        DispatchQueue.main.async {
//            print("Time :" + String(UIApplication.shared.backgroundTimeRemaining))
//
//            self.scheduleAlarmForDate()
//        }
//
//        var bgTask  : UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
//        bgTask = application.beginBackgroundTask(expirationHandler: {
//            application.endBackgroundTask(bgTask)
//            print("back")
//        })
//
//        print("applicationWillTerminate ")
    }
    
    func scheduleAlarmForDate(){
        Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true, block: {_ in
            //            UserDefaults.standard.setValue(Date(), forKey: "date")
            print("Time :" + String(UIApplication.shared.backgroundTimeRemaining))
            
        })
        // Create a new notification.
        //        let content = UNMutableNotificationContent()
        //        content.title = NSString.localizedUserNotificationString(forKey: "Elon said:", arguments: nil)
        //        content.body = NSString.localizedUserNotificationString(forKey: "Hello Tom！Get up, let's play with Jerry!", arguments: nil)
        //        content.sound = UNNotificationSound.default()
        //        content.categoryIdentifier = "com.elonchan.localNotification"
        //        // Deliver the notification in five seconds.
        //        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 60.0, repeats: true)
        //        let request = UNNotificationRequest.init(identifier: "FiveSecond", content: content, trigger: trigger)
        //
        //        // Schedule the notification.
        //        let center = UNUserNotificationCenter.current()
        //        center.add(request)
        
    }
    
    
}

