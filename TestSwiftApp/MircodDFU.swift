import UIKit
import iOSDFULibrary
import CoreBluetooth
@objc  public protocol MircodDFUDelegate {
    
    /*!
     * @discussion Gives detailed information about the progress and speed of transmission. This method is always called at least two times (for 0% and 100%)
     * @param part number of part that is currently being transmitted.
     * @param totalParts total number of parts that are to be send.
     * @param progress the current progress of uploading the current part in percentage (values 0-100).
     * @param currentSpeedBytesPerSecond the current speed in bytes per second
     * @param avgSpeedBytesPerSecond the average speed in bytes per second
     */
    @objc optional  func MircodDfuProgressDidChangeFor(part : NSInteger, totalParts: NSInteger , progress: NSInteger , currentSpeedBytesPerSecond : Double , avgSpeedBytesPerSecond:Double)
    /*!
     * @discussion  This method is called whenever a new log entry is to be saved. The logger implementation should save this or present it to the user.
     * It is NOT safe to update any UI from this method as multiple threads may log.
     * @param message the message
     */
    @objc optional func newLog(message: String)
    /*!
     * @discussion Called after an error occurred.The device will be disconnected
     * @param error the error code
     */
    @objc optional func errorToUpdate(error : Error?);
    /*!
     * @param peripheral instance of MircodPeripheral
     * @discussion Invoke method when end updating peripheral
     */
    @objc optional func endLoadingWithPeripheral(peripheral : MircodPeripheral)
    /*!
     * DFU operation has been aborted
     */
    @objc optional func didAbort()
    /*!
     * DFU operation has been paused
     */
    @objc optional func didPause()
    /*!
     * DFU operation has been resume
     */
    @objc optional func didResume()
    
}
extension String {
    //get substring from range
    subscript(_ range: NSRange) -> String {
        let start = self.index(self.startIndex, offsetBy: range.lowerBound)
        let end = self.index(self.startIndex, offsetBy: range.upperBound)
        let subString = self[start..<end]
        return String(subString)
    }
}
/*!
 *  @class MircodScanner
 *  @discussion Represents class from mircod company for update firmware of MircodPeripheral.
 *  IMPORTANT : MircodDFU take away control of all devices, necessary to invoke method 'stop'
 */
open class MircodDFU: NSObject {
    private var hiddenDelegate: PrivateMircodDFU?
    /*!
     *  @property delegate
     *  @discussion The delegate object that will receive central events
     */
    open var delegate : MircodDFUDelegate?{
        get {
            return self.hiddenDelegate!.delegate
        }
    }
    /*!
     *  @property delegate
     *  @discussion if sensor is aborted return true
     */
    public var isAborted: Bool {
        get {
            return self.hiddenDelegate?.dfuController?.aborted ?? false;
        }
    }
    /*!
     *  @property dfuperipheral  instance of MircodPeripheral
     *  @discussion Peripheral which selected to update
     */
    public var dfuperipheral : MircodPeripheral?{
        get {
            return self.hiddenDelegate?.dfuperipheral
        }
    }
    /*!
     *  @property dfuController instance of DFUServiceController
     *  @discussion Peripheral which selected to update
     */
    public   var dfuController: DFUServiceController?{
        get {
            return self.hiddenDelegate?.dfuController
        }
    }
    /*!
     *  @property scanner instance of MircodScanner
     *  @discussion scanner for work with device
     */
    public   var scanner: MircodScanner?{
        get {
            return self.hiddenDelegate?.scanner
        }
    }
    
    //MARK: - initicialization
    
    public override init() {
        super.init()
    }
    
    public convenience  init(withDelegate delegate : MircodDFUDelegate , scanner : MircodScanner ){
        self.init()
        hiddenDelegate = PrivateMircodDFU.init(withDelegate: delegate, scanner: scanner, outer: self)
        
    }
    
    //MARK: - DFU
    
    /*!
     @discussion Start update peripheral
     @param peripheral is instence of MircodPeripheral
     @param URL  is NSURL of file with the firmware.
     */
    public func updatePeripheral(peripheral : MircodPeripheral! ,  withArchiveURL URL:URL!) {
        hiddenDelegate?.updatePeripheral(peripheral: peripheral, withArchiveURL: URL)
    }
    /*!
     * @discussion Call this method to pause uploading during the transmition process. The transmition can be resumed
     only when connection remains.
     */
    public func pause(){
        hiddenDelegate?.pause()
    }
    /*!
     * @discussion stop loading firmware and return the control to parent
     */
    public func stop(){
        hiddenDelegate?.stop()
    }
    /*!
     * @discussion Aborts the upload. The phone will disconnect from peripheral. The peripheral will try to
     recover the last firmware. Might, restart in the Bootloader mode if the application has been
     removed.
     */
    public func abort(){
        hiddenDelegate?.abort()
    }
    /*!
     *  @discussion Call this method to resume the paused transffer, otherwise does nothing.
     */
    public func resume(){
        hiddenDelegate?.resume()
    }
    /*!
     * @discussion Starts again aborted DFU operation
     */
    public func restart(){
        hiddenDelegate?.restart()
    }
}



private class PrivateMircodDFU:  NSObject,DFUServiceDelegate, DFUProgressDelegate, LoggerDelegate, MircodScannerDelegate, MircodPeripheralDelegate {
    //TODO: realization of the method
    func newSensor(_ sensor: MircodSensor, peripheral: MircodPeripheral) {
        
    }
    
    
    open var delegate : MircodDFUDelegate?
    var secureDFU = false
    var isAborted: Bool {
        get {
            return self.dfuController?.aborted ?? false;
        }
    }
    var dfuperipheral : MircodPeripheral?
    open var delegateScanner : MircodScannerDelegate?
    open var delegatePeripheral: MircodPeripheralDelegate?
    var dfuController: DFUServiceController?
    var scanner: MircodScanner?
    var selectedFirmware: DFUFirmware?
    var selectedFileURL : URL?
    var  macadress: String!
    var  isLoading = false
    var  isStopped = false
    
    
    //MARK: - initicialization
    
    override init() {
        super.init()
    }
    
    convenience  init(withDelegate delegate : MircodDFUDelegate , scanner : MircodScanner , outer: MircodDFU){
        self.init()
        self.delegate = delegate
        self.scanner = scanner
        delegateScanner = scanner.scannerDelegate
        delegatePeripheral = scanner.peripheralDelegate
        
    }
    
    //MARK: - DFU
    
    func updatePeripheral(peripheral : MircodPeripheral! ,  withArchiveURL URL:URL!) {
        isLoading = true
        delegatePeripheral = peripheral.delegate
        delegateScanner = scanner?.scannerDelegate
        scanner?.scannerDelegate = self
        scanner?.peripheralDelegate = self
        dfuperipheral = peripheral
        macadress = peripheral.macadress
        selectedFileURL = URL
        if peripheral.state == .Main {
            dfuperipheral?.reboot()
        }
        else if peripheral.state == .Bootloader {
            checkURL()
        }
        else {
            print("Bad device")
        }
        
    }
    func EqualsSelfPeripheralWithPeripheral(peripheral :MircodPeripheral) -> Bool {
        guard peripheral.macadress.count > 1 else {
            return false
        }
        let start = macadress.index(macadress.startIndex, offsetBy: 1)
        let end = macadress.index(macadress.endIndex, offsetBy: -1)
        
        let str1 =  macadress[start..<end]
        
        let start2 = peripheral.macadress.index(peripheral.macadress.startIndex, offsetBy: 1)
        let end2 = peripheral.macadress.index(peripheral.macadress.endIndex, offsetBy: -1)
        
        let str2 = peripheral.macadress[start2..<end2]
        
        print(str1 + " and " + str2)
        return   str1 == str2
    }
    
    func secureDFUMode(secureDFU : Bool){
        self.secureDFU = secureDFU
    }
    func securesetCentralManagerDFUMode(centralManager :CBCentralManager){
        scanner?.updateManager(manager:centralManager)
    }
    func setTargetPeripheral(targetPeripheral : CBPeripheral){
        dfuperipheral?.peripheral = targetPeripheral
    }
    
    
    func pause(){
        if dfuController != nil  && !((self.dfuController?.aborted)!) {
            isStopped = true
            dfuController?.pause()
        }
    }
    func stop(){
        end()
    }
    func abort(){
        if let abort =  dfuController?.abort() {
            print("Was aborted ", abort == true ? "YES"  :  "NO")
        }
    }
    func resume(){
        if isStopped{
            dfuController?.resume()
            isStopped = false
        }
    }
    func restart(){
        dfuController?.restart()
    }
    
    func end(){
        if isLoading || isAborted || isStopped {
            abort()
        }
        dfuperipheral?.delegate = delegatePeripheral
        scanner?.peripheralDelegate = delegatePeripheral
        scanner?.scannerDelegate  =  delegateScanner
        delegatePeripheral = nil
        delegateScanner = nil
        selectedFileURL = nil
        selectedFirmware = nil
        macadress = nil
        secureDFU = false
        isLoading = false
        delegate?.endLoadingWithPeripheral!(peripheral: dfuperipheral!)
    }
    
    func  stopLoad() {
        // Forget the controller when DFU is done
        dfuController = nil
        scanner?.updateManager(manager: (scanner?.manager)!)
        scanner?.cancelConnect(withPeripheral: dfuperipheral!)
        scanner?.stopScanDevices()
        scanner?.startScanDevices([Category.BrushCategory, Category.DexcomCategory, Category.ERRORCategory, Category.InsupenCategory, Category.PatchCategory, Category.PatchCategory, Category.TempPatchCategory, Category.ToolCategory])
    }
    
    
    func checkURL(){
        if let URL = selectedFileURL {
            self.selectedFirmware = DFUFirmware.init(urlToZipFile: URL as URL)
            if self.selectedFirmware != nil {
                startDFUProcess()
            }
            else {
                print("Error : The archive isn't found")
            }
        }
        else {
            print("Error : URL is nil")
        }
    }
    
    func startDFUProcess(){
        if dfuperipheral == nil {
            print("No DFU peripheral was set");
            return;
        }
        let dfuInitiator = DFUServiceInitiator.init(centralManager: (scanner?.manager)!, target: (dfuperipheral?.peripheral)!)
        dfuInitiator.delegate = self
        dfuInitiator.progressDelegate = self
        dfuInitiator.logger = self
        
        // Please, read the field documentation before use.
        dfuInitiator.enableUnsafeExperimentalButtonlessServiceInSecureDfu = true;
        dfuController = dfuInitiator.with(firmware: selectedFirmware!).start()
        
    }
    
    //MARK:  - MircodScannerDelegate
    
    func didConnectToPeripheral(_ peripheral: MircodPeripheral) {
        if EqualsSelfPeripheralWithPeripheral(peripheral: peripheral){
            peripheral.delegate = self
            dfuperipheral = peripheral
        }
        else {
            delegateScanner?.didConnectToPeripheral(peripheral)
        }
    }
    func didFailConnectToPeripheral(_ peripheral: MircodPeripheral, withError error: Error?) {
        print("error __________ " + String.init(describing: error));
        if dfuperipheral == peripheral {
            scanner?.cancelConnect(withPeripheral: peripheral)
        }
        end()
        delegate?.errorToUpdate!(error: error)
    }
    
    func disconnectPeripheral(_ peripheral: MircodPeripheral, error:Error?) {
        if  EqualsSelfPeripheralWithPeripheral(peripheral: peripheral) {
            if (scanner?.selectedPeripherals.contains(peripheral))! {
                checkURL()
            }
            else {
                scanner?.connect(toPeripheral: peripheral, withTimeout: nil, withDelegate: self)
                scanner?.timerInvalidate()
            }
        }
        else {
            
            delegateScanner?.disconnectPeripheral(peripheral, error:error)
        }
    }
    
    
    func newPeripheral(_ peripheral: MircodPeripheral) {
        if  EqualsSelfPeripheralWithPeripheral(peripheral: peripheral) {
            if isLoading {
                self.dfuperipheral = peripheral
            }
            scanner?.connect(toPeripheral: peripheral, withTimeout: nil, withDelegate: self)
        }
        else {
            delegateScanner?.newPeripheral(peripheral)
        }
    }
    
    func bluetoothManagerDidUpdateState(_ state: MicodScannerState) {
        
    }
    //MARK:  - MircodPeripheralDelegate
    func listOfSensorsIsLoadedFromPeripheral(_ peripheral: MircodPeripheral) {
        if EqualsSelfPeripheralWithPeripheral(peripheral: peripheral){
            if self.isLoading{
                checkURL()
            }
            else{
                dfuperipheral = peripheral;
                end()
            }
        }
        else {
            delegatePeripheral?.listOfSensorsIsLoadedFromPeripheral(peripheral)
            
        }
    }
    
    
    
    func newValueFromPeripheral(_ peripheral: MircodPeripheral, value: Double, sensor: MircodSensor) {
        
    }
    //
    //    func newDateValueFromPeripheral(_ peripheral: MircodPeripheral, value: Double, sensor: MircodSensor, date: Date, progress: Double) {
    //
    //
    //    }
    
    func newDateValueFromPeripheral(_ peripheral: MircodPeripheral, values: [Double], sensor: MircodSensor, date: Date, progress: Double) {
        
    }
    
    
    
    func timeOfTheTimerLeftFromPeripheral(_ peripheral: MircodPeripheral) {
        
    }
    
    //MARK: DFU FRame
    func dfuStateDidChange(to state: DFUState) {
        switch state {
        case .disconnecting:
            print("Disconnect")
            break
        case .completed:
            isLoading = false
            stopLoad()
            print("Completed or disconnect")
            break
        case .aborted:
            delegate?.didAbort!()
            print("Abort")
            isLoading = false
            stopLoad()
            break
        default: break
            
        }
        print("\n \nChanged state to: \(state.description())")
        
        
    }
    
    func dfuError(_ error: DFUError, didOccurWithMessage message: String) {
        print(message)
        
    }
    
    
    func logWith(_ level: LogLevel, message: String) {
        delegate?.newLog!(message: message)
    }
    
    func dfuProgressDidChange(for part: Int, outOf totalParts: Int, to progress: Int, currentSpeedBytesPerSecond: Double, avgSpeedBytesPerSecond: Double) {
        delegate?.MircodDfuProgressDidChangeFor!(part: part, totalParts: totalParts, progress: progress, currentSpeedBytesPerSecond: currentSpeedBytesPerSecond, avgSpeedBytesPerSecond: avgSpeedBytesPerSecond)
    }
    
}
