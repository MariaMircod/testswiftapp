//
//  Bridge.h
//  TestSwiftApp
//
//  Created by Мария Тимофеева on 15.09.17.
//  Copyright © 2017 Mircod. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TestSwiftApp-Bridging-Header.h"
#import "CoreBluetooth/CoreBluetooth.h"
@interface Bridge : NSObject
-(NSDictionary<NSString*, NSNumber*>*)updateECG:(int)data;
-(NSDictionary<NSString*, NSNumber*>*)updatePPGRed:(double)Red IR:(double)IR;
-(NSArray<NSNumber*>*)proccessData:(NSData*)data;
-(NSDictionary<NSString*, NSNumber*>*)proccessYawPitchRollData:(NSData*)data;
-(NSDictionary<NSString*, NSNumber*>*)proccessAccelerationData:(NSData*)data;


@end
