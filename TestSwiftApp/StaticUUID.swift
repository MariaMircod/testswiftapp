//
//  StaticUUID.swift
//  MircodIOSFrameworkSwift
//
//  Created by Maria on 19.09.2018.
//  Copyright © 2018 Mircod. All rights reserved.
//

import Foundation


let BOOTLOADER_STATE_SERVICES_UUID =  "FE59"
let BOOTLOADER_SERVICES_UUID = "FFD0"
let ACTION_UUID = "1773AC00-AC59-11E8-9A57-8EAE33A81C82"
let ACTION_BULAT_UUID = "A6BA0002-1B06-49C1-9B5A-58E937AE3094"
let STATUS_UUID = "A6BA0003-1B06-49C1-9B5A-58E937AE3094"
let MIRCOD_SERVICE_UUID = "A6BA0001-1B06-49C1-9B5A-58E937AE3094"
let Battery_SERVICES_UUID = "180F"
let DEVICE_INFORMATION_SERVICES_UUID = "180A"
let HUMMER_UUID = "D02A"
let BRUSH_UUID = "D01A"
let PATCH_UUID = "FFE0"
let TEMP_PATCH_UUID = "D03A"
let INSUPEN_UUID = "D2970001-FFE3-4A85-BC6D-4A7B24C5D054"
let CARDIOSTUDY_UUID = "1773D05A-AC59-11E8-9A57-8EAE33A81C82"
