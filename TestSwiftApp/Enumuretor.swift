//
//  Enamuretor.swift
//  TestSwiftApp
//
//  Created by Maria on 19.09.2018.
//  Copyright © 2018 Mircod. All rights reserved.
//

import Foundation
//
//  Enumerator.swift
//  MircodIOSFrameworkSwift
//
//  Created by Maria on 19.09.2018.
//  Copyright © 2018 Mircod. All rights reserved.
//

import Foundation


@objc public enum BatteryStatus : UInt16 {
    case on = 0
    case charging = 1
    case lowBattery = 3
    case none = 255
}

@objc public enum PowerStatus : UInt16 {
    case lowPower = 0
    case active = 1
    case none = 255
}

@objc public enum RecordingStatus : UInt16 {
    case notRecording = 0
    case isRecording = 1
    case none = 255
}



public enum MicodScannerState : Int {
    case unknown
    case resetting
    case unsupported
    case unauthorized
    case off
    case on
}


/**
 *  @brief Enumeration of command for device
 **/
public enum ActionComand{
    case start
    case stop
    case startRecord
    case stopRecord
    case bootlooder
    case updateTime
}
/**
 *  @brief Enumeration of device state
 **/
public enum DeviceState{
    case  ERRORState
    case  Main
    case  Bootloader
}
/**
 *  @brief Enumeration of temp patch color
 **/
public enum TempPatchColor {
    case ERRORColor
    case red
    case blue
    case white
    
    public var description : String {
        switch self {
        case .red:
            return "red"
        case .blue:
            return "blue"
        case .white:
            return "white"
        default:
            return "no color"
        }
    }
}
/**
 *  @brief Enumeration of tool type
 **/
public enum ToolType {
    case Hammer
    case Wrech
    case ERROR
    
    
    public  var description: String {
        switch self {
        case .Hammer:
            return "Hammer"
        case .Wrech:
            return "Wrech"
        default:
            return "no type"
        }
    }
}

/**
 *  @brief Enumeration of tool type
 **/
public enum InsupenType {
    case LantusSolostar
    case ToujeoSolostar
    case TresibaFlexTouch
    case NovoRapidFlexPen
    case HumalogKwikpen
    case ApidraSolostar
    case FlexTouch
    case BasaglarKwikpen
    case LevemirFlexTouch
    
    case ERROR
    
    
    public  var description: String {
        switch self {
        case .LantusSolostar:
            return "Lantus Solostar"
        case .ToujeoSolostar:
            return "Toujeo Solostar"
        case .TresibaFlexTouch:
            return "TresibaFlexTouch"
        case .NovoRapidFlexPen:
            return "NovoRapid FlexPen"
        case .HumalogKwikpen:
            return "Humalog Kwikpen"
        case .ApidraSolostar:
            return "Apidra Solostar"
        case .FlexTouch:
            return "FlexTouch"
        case .BasaglarKwikpen:
            return "Basaglar Kwikpen"
        case .LevemirFlexTouch:
            return "Levemir FlexTouch"
        default:
            return "no type"
        }
    }
    
    
    public  var maxValue: Int {
        switch self {
        case .LantusSolostar , .TresibaFlexTouch , .NovoRapidFlexPen , .HumalogKwikpen :
            return 300
        case .ToujeoSolostar:
            return  450
        default:
            return 0
        }
    }
}

/**
 *  @brief Enumeration of category device
 **/
public enum Category {
    case PatchCategory
    case ToolCategory
    case BrushCategory
    case TempPatchCategory
    case DexcomCategory
    case InsupenCategory
    case CardioStudyCategory
    case ERRORCategory
}

/**
 *  @brief Enumeration of device type
 **/
public enum DeviceType {
    case Patch
    case Tool(type: ToolType)
    case Brush
    case Dexcom
    case Insupen(type: InsupenType)
    case TempPatch(color: TempPatchColor)
    case CardioStudy
    case ERRORType
    
    public var name: String!{
        switch self {
        case .TempPatch(_) : return "TempPatch"
        case .Patch:return "Patch"
        case .Dexcom:return "Dexcom"
        case .Insupen(_):return "Insupen"
        case .Tool(_) : return "Tool"
        case .Brush:return "Brush"
        case .CardioStudy:return "CardioStudy"
        case .ERRORType: return "ERROR"
        }
    }
    
    public var category : Category {
        switch self {
        case .TempPatch(_) : return .TempPatchCategory
        case .Patch:return .PatchCategory
        case .Dexcom:return .DexcomCategory
        case .Insupen(_):return .InsupenCategory
        case .Tool(_) :return .ToolCategory
        case .Brush:return .BrushCategory
        case .CardioStudy:return .CardioStudyCategory
        default:
            return .ERRORCategory
        }
    }
    
    public var parametr: Any?{
        switch self {
        case .TempPatch(let color) : return color
        case .Patch:return nil
        case .Tool(let type) : return type
        case .Brush: return nil
        case .Dexcom: return nil
        case .Insupen(let type): return type
        case .CardioStudy: return nil
        default : return  nil
        }
    }
}
