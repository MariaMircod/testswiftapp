//
//  BackgroundBLEManager.swift
//  TestSwiftApp
//
//  Created by Мария Тимофеева on 30.10.2017.
//  Copyright © 2017 Mircod. All rights reserved.
//

import UIKit
import Foundation
class BackgroundBLEManager: NSObject , CBCentralManagerDelegate , CBPeripheralDelegate {
    
    static let instance = BackgroundBLEManager()
    static let BACKGROUND_TIMER = 150.0 // restart location manager every 150 seconds
    static let UPDATE_SERVER_INTERVAL = 60 * 60 // 1 hour - once every 1 hour send location to server
    private var per : CBPeripheral?
    let manager = CBCentralManager()
    var timer: Timer?
    var currentBgTaskId : UIBackgroundTaskIdentifier?
    var lastLocationDate : NSDate = NSDate()
    
    private override init(){
        super.init()
        manager.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    @objc  func applicationEnterBackground(){
        print("applicationEnterBackground")
        start()
    }
    
  public  func start(){
    print("start")
        if let iden = UserDefaults.standard.value(forKey: "peripheral") {
            let arr = manager.retrievePeripherals(withIdentifiers: [UUID.init(uuidString: iden as! String)!])
            per = arr[0]
            manager.connect(per!, options: nil)
            
        }
    }
  @objc  func restart (){
        timer?.invalidate()
        timer = nil
        start()
    }

    
    func isItTime(now:Date) -> Bool {
        let timePast = now.timeIntervalSince(lastLocationDate as Date)
        let intervalExceeded = Int(timePast) > BackgroundBLEManager.UPDATE_SERVER_INTERVAL
        return intervalExceeded;
    }
    
    func sendLocationToServer(value : Double){
        //TODO
    }
    
    func beginNewBackgroundTask(){
        var previousTaskId = currentBgTaskId;
        currentBgTaskId = UIApplication.shared.beginBackgroundTask(expirationHandler: {
//            FileLogger.log("task expired: ")
        })
        if let taskId = previousTaskId{
            UIApplication.shared.endBackgroundTask(taskId)
            previousTaskId = UIBackgroundTaskIdentifier(rawValue: convertFromUIBackgroundTaskIdentifier(UIBackgroundTaskIdentifier.invalid))
        }
        
        timer = Timer.scheduledTimer(timeInterval: BackgroundBLEManager.BACKGROUND_TIMER, target: self, selector: #selector(self.restart),userInfo: nil, repeats: false)
    }
    
    //Delegate mathods
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOff:
            manager.stopScan()
        case .poweredOn :
            start()
       
        default:
            break
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.discoverServices(nil)

    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in   peripheral.services!{
        peripheral.discoverCharacteristics(nil, for:service)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for aChar in service.characteristics! {
            
           peripheral.setNotifyValue(true, for: aChar)
        }
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if(timer==nil){
            // The locations array is sorted in chronologically ascending order, so the
            // last element is the most recent
            //            guard let location = locations.last else {return}
            
            beginNewBackgroundTask()
            manager.stopScan()
            
            let now = Date()
            if(isItTime(now: now)){
//                TODO: Every n minutes do whatever you want with the new location. Like for example sendLocationToServer(location, now:now)
                
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIBackgroundTaskIdentifier(_ input: UIBackgroundTaskIdentifier) -> Int {
	return input.rawValue
}
