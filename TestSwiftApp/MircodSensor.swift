import Foundation
import CoreBluetooth
/**
 *  @brief Enumeration of sensor uuid
 **/
public enum SensorUUID : String  {
    case sensorECG               = "FFE1"
    case sensorGSR               = "FFE2"
    case sensorHPA               = "FFE3"
    case sensorTEMPERATUREAIR    = "FFE4"
    case sensorLeadOFF           = "FFE5"
    case sensorTEMPERATUREBODY   = "FFE6"
    case sensorYAWPITCHROLL      = "FFE7"
    case sensorSPO2              = "FFE8"
    case sensorOPTICAL_LOD       = "FFE9"
    case sensorBYTE_STREAM       = "FFEA"
    case ERROR                   = "FFFF"
    case sensorHITS              = "FFEC"
    case sensorBOOTLOADER        = "FFD5"
    case sensorBATTERY           = "2A19"
    case sensorACCLERATION       = "FFEE"
    case sensorTIME_TEMP         = "FFED"
    case sensorGLUCOSE           = "EEE2"
    case sensorINSULIN           = "D2970002-FFE3-4A85-BC6D-4A7B24C5D054"
    case sensorTEMPERATURE_INSUPEN = "D2970003-FFE3-4A85-BC6D-4A7B24C5D054"
    case sensorRESIDUE           = "EEE3"
    case sensorMULTI_ECG         = "1773AC01-AC59-11E8-9A57-8EAE33A81C82"
    case sensorSTEPS             = "1773AC02-AC59-11E8-9A57-8EAE33A81C82"
    case sensorSPEED             = "1773AC03-AC59-11E8-9A57-8EAE33A81C82"
    case sensorMOVEMENT          = "1773AC04-AC59-11E8-9A57-8EAE33A81C82"
    case sensorECG_PROCESS       = "F8E9C004-4753-4A8D-9A57-8E56BDA8AB14"
    
    public func convertCBUUIDTOSensorUUID(_ uuid : CBUUID) -> SensorUUID {
        let stringID : String = uuid.uuidString
        if let sensor = SensorUUID.init(rawValue:stringID){
            return sensor
        }
        else {
            return .ERROR
        }
    }
}
/**
 *  @brief Enumeration of sensor names
 **/
public enum SensorName : String   {
    case ECG                 = "ECG"
    case MULTI_ECG           = "MULTI_ECG"
    case GSR                 = "GSR"
    case YAW                 = "YAW"
    case PITCH               = "PITCH"
    case ROLL                = "ROLL"
    case TEMPERATUREAIR      = "TEMPERATUREAIR"
    case HPA                 = "HPA"
    case TEMPERATUREBODY     = "TEMPERATUREBODY"
    case SPO2                = "SPO2"
    case LEADOFF             = "LEADOFF"
    case ECG_HR              = "ECG_HR"
    case ECG_HRV             = "ECG_HRV"
    case ECG_BreathingRate   = "ECG_BreathingRate"
    case ECG_RRInterval      = "ECG_RRInterval"
    case SPO2_HR             = "SPO2_HR"
    case SPO2_HRV            = "SPO2_HRV"
    case OPTICAL_LOD         = "OPTICAL_LOD"
    case BYTE_STREAM         = "BYTE_STREAM"
    case YAWPITCHROLL        = "YAWPITCHROLL"
    case HITS                = "HITS"
    case BOOTLOADER          = "BOOTLOADER"
    case ACCLERATION         = "ACCLERATION"
    case ACCLERATION_X       = "ACCLERATION X"
    case ACCLERATION_Y       = "ACCLERATION Y"
    case ACCLERATION_Z       = "ACCLERATION Z"
    case TIME_TEMP           = "TIME_TEMP"
    case GLUCOSE             = "GLUCOSE"
    case INSULIN             = "INSULIN"
    case RESIDUE             = "RESIDUE"
    case TEMPERATURE_INSUPEN = "TEMPERATURE_INSUPEN"
    case MOVEMENT            = "MOVEMENT"
    case STEPS               = "STEPS"
    case SPEED               = "SPEED"
    case ECG_PROCESS         = "ECG_PROCESS"
    
    case BATTERY             = "BATTERY"
    case ERROR               = "ERROR"
}

open class MircodSensor : NSObject  {
    
    
    open  var name :SensorName
    /*!
     *  var uuid instans of SensorUUID
     *  @discussion uuid from the characteristic in SensorUUID format.
     */
    open var uuid : SensorUUID
    /*!
     *  var characteristic instans of CBCharacteristic
     *  @discussion Sensor characteristic.
     *  IMPORTANT: from one characteristic can comes for data for several sensors.
     */
    open  var  characteristic : CBCharacteristic?
    /*!
     *  @property isObserve is bool type
     *  @discussion if sensor is observe return true
     */
    open   var  isObserve = false
    /*!
     *  @property isWritable is bool type
     *  @discussion id sensor is availlable for  write
     */
    open var isWritable : Bool{
        get {
            return  ((characteristic?.properties.contains(.write))! || (characteristic?.properties.contains(.writeWithoutResponse))!) ? true : false
        }
    }
    public override init() {
        self.name = .ERROR
        self.uuid  = .ERROR
        self.characteristic = nil
    }
    
    public init(sensorName : SensorName){
        self.name = sensorName
        var uuid : SensorUUID
        switch name {
        case .ECG , .ECG_HR, .ECG_HRV , .ECG_RRInterval ,.ECG_BreathingRate:    uuid = .sensorECG
        case .MULTI_ECG:                                                        uuid = .sensorMULTI_ECG
        case .SPO2 , . SPO2_HR, . SPO2_HRV:                                     uuid = .sensorSPO2
        case .GSR:                                                              uuid = .sensorGSR
        case .YAW, .PITCH , .ROLL, .YAWPITCHROLL:                               uuid = .sensorYAWPITCHROLL
        case .TEMPERATUREAIR :                                                  uuid = .sensorTEMPERATUREAIR
        case .TEMPERATUREBODY:                                                  uuid = .sensorTEMPERATUREBODY
        case .HPA :                                                             uuid = .sensorHPA
        case .LEADOFF :                                                         uuid = .sensorLeadOFF
        case .OPTICAL_LOD :                                                     uuid = .sensorOPTICAL_LOD
        case .BYTE_STREAM :                                                     uuid = .sensorBYTE_STREAM
        case .BOOTLOADER :                                                      uuid = .sensorBOOTLOADER
        case .HITS :                                                            uuid = .sensorHITS
        case .BATTERY :                                                         uuid = .sensorBATTERY
        case .ACCLERATION_X ,.ACCLERATION_Y ,.ACCLERATION_Z :                   uuid = .sensorACCLERATION
        case .TIME_TEMP:                                                        uuid = .sensorTIME_TEMP
        case .GLUCOSE:                                                          uuid = .sensorGLUCOSE
        case .INSULIN:                                                          uuid = .sensorINSULIN
        case .RESIDUE:                                                          uuid = .sensorRESIDUE
        case .TEMPERATURE_INSUPEN:                                              uuid = .sensorTEMPERATURE_INSUPEN
        case .MOVEMENT:                                                         uuid = .sensorMOVEMENT
        case .STEPS:                                                            uuid = .sensorSTEPS
        case .SPEED:                                                            uuid = .sensorSPEED
        case .ECG_PROCESS:                                                      uuid = .sensorECG_PROCESS
        default:
            uuid = .ERROR
        }
        self.uuid = uuid
        
    }
    
    public init(sensorUuid uuid : SensorUUID) {
        self.uuid = uuid
        var name : SensorName
        switch uuid {
        case .sensorECG             :  name = .ECG
        case .sensorMULTI_ECG       :  name = .MULTI_ECG
        case .sensorSPO2            :  name = .SPO2
        case .sensorGSR             :  name = .GSR
        case .sensorYAWPITCHROLL    :  name = .YAWPITCHROLL
        case .sensorTEMPERATUREAIR  :  name = .TEMPERATUREAIR
        case .sensorTEMPERATUREBODY :  name = .TEMPERATUREBODY
        case .sensorHPA             :  name = .HPA
        case .sensorLeadOFF         :  name = .LEADOFF
        case .sensorOPTICAL_LOD     :  name = .OPTICAL_LOD
        case .sensorBYTE_STREAM     :  name = .BYTE_STREAM
        case .sensorBOOTLOADER      :  name = .BOOTLOADER
        case .sensorHITS            :  name = .HITS
        case .sensorBATTERY         :  name = .BATTERY
        case.sensorACCLERATION      :  name = .ACCLERATION
        case.sensorTIME_TEMP        :  name = .TIME_TEMP
        case.sensorGLUCOSE          :  name = .GLUCOSE
        case.sensorINSULIN          :  name = .INSULIN
        case.sensorRESIDUE          :  name = .RESIDUE
        case.sensorTEMPERATURE_INSUPEN     :  name = .TEMPERATURE_INSUPEN
        case.sensorMOVEMENT         :  name = .MOVEMENT
        case.sensorSTEPS            :  name = .STEPS
        case.sensorSPEED            :  name = .SPEED
        case.sensorECG_PROCESS      :  name = .ECG_PROCESS
            
        default:
            name = .ERROR
        }
        self.name = name
    }
    
    public convenience init(cbuuid : CBUUID){
        let id = SensorUUID.sensorECG
        let sensorUUID = id.convertCBUUIDTOSensorUUID(cbuuid)
        self.init(sensorUuid: sensorUUID)
    }
    
    public convenience init(characteristic : CBCharacteristic){
        
        if let descriptors = characteristic.descriptors {
            self.init(descriptors: descriptors)
            if name == .ERROR{
                if let sensor = getSensorsFrom(cbuuid: characteristic.uuid).first{
                    name = sensor.name
                    uuid = sensor.uuid
                }
            }
            else {
                
            }
        }
        else {
            self.init(cbuuid: characteristic.uuid)
        }
        self.characteristic = characteristic
    }
    
    public convenience init(descriptors : [CBDescriptor]){
        self.init()
        for descriptor in descriptors {
            if let value = descriptor.value{
                
                if value is String {
                    let nameStr = value as! String
                    switch nameStr{
                    case "Temperature" :
                        name = .TEMPERATURE_INSUPEN
                        uuid  = .sensorTEMPERATURE_INSUPEN
                        //                    case "Injection" :
                        //                        name = .INSULIN
                    //                        uuid  = .sensorINSULIN
                    default : continue
                    }
                }
            }
        }
    }
    
    
    /**
     *@param cbuuid of instance CBUUID
     *@return array of MircodSensor with the appropriate uuid.
     */
    public func getSensorsFrom(cbuuid : CBUUID) -> [MircodSensor]{
        return getSensorsFrom(stringUuid: cbuuid.uuidString)
    }
    
    /**
     *@param stringUuid of instance String
     *@return array of MircodSensor with the appropriate uuid.
     */
    public func getSensorsFrom(stringUuid : String) -> [MircodSensor]{
        if let uuid = SensorUUID.init(rawValue:stringUuid) {
            return getSensorsFrom(uuid: uuid)
        }
        else {
            return [MircodSensor.init(sensorName: .ERROR)];
        }
    }
    /**
     *@param uuid of instance SensorUUID
     *@return array of MircodSensor with the appropriate uuid.
     */
    func getSensorsFrom(uuid : SensorUUID) -> [MircodSensor]{
        var sensors  = [MircodSensor] ()
        switch uuid {
        case .sensorECG:
            sensors+=[MircodSensor.init(sensorName: .ECG),
                      MircodSensor.init(sensorName: .ECG_HR),
                      MircodSensor.init(sensorName: .ECG_HRV),
                      MircodSensor.init(sensorName: .ECG_RRInterval),
                      MircodSensor.init(sensorName: .ECG_BreathingRate)]
        case .sensorMULTI_ECG:
            sensors+=[MircodSensor.init(sensorName: .MULTI_ECG)]
        case .sensorGSR:
            sensors+=[ MircodSensor.init(sensorName: .GSR)]
        case .sensorHPA:
            sensors+=[ MircodSensor.init(sensorName: .HPA)]
        case .sensorYAWPITCHROLL:
            sensors+=[ MircodSensor.init(sensorName: .YAW),
                       MircodSensor.init(sensorName: .PITCH),
                       MircodSensor.init(sensorName: .ROLL)]
        case .sensorTEMPERATUREAIR:
            sensors+=[ MircodSensor.init(sensorName: .TEMPERATUREAIR)]
        case .sensorTEMPERATUREBODY:
            sensors+=[ MircodSensor.init(sensorName: .TEMPERATUREBODY)]
        case .sensorLeadOFF:
            sensors+=[ MircodSensor.init(sensorName: .LEADOFF)]
        case .sensorOPTICAL_LOD:
            sensors+=[ MircodSensor.init(sensorName: .OPTICAL_LOD)]
        case .sensorBYTE_STREAM:
            sensors+=[ MircodSensor.init(sensorName: .BYTE_STREAM)]
        case .sensorSPO2 :
            sensors+=[MircodSensor.init(sensorName: .SPO2)]
            sensors+=[MircodSensor.init(sensorName: .SPO2_HR)]
            sensors+=[MircodSensor.init(sensorName: .SPO2_HRV)]
        case .sensorBOOTLOADER:
            sensors+=[MircodSensor.init(sensorName: .BOOTLOADER)]
        case .sensorHITS:
            sensors+=[MircodSensor.init(sensorName: .HITS)]
        case .sensorBATTERY:
            sensors+=[MircodSensor.init(sensorName: .BATTERY)]
        case .sensorACCLERATION :
            sensors+=[MircodSensor.init(sensorName: .ACCLERATION_X)]
            sensors+=[MircodSensor.init(sensorName: .ACCLERATION_Y)]
            sensors+=[MircodSensor.init(sensorName: .ACCLERATION_Z)]
        case .sensorTIME_TEMP:
            sensors+=[MircodSensor.init(sensorName: .TIME_TEMP)]
        case .sensorGLUCOSE:
            sensors+=[MircodSensor.init(sensorName: .GLUCOSE)]
        case .sensorINSULIN:
            sensors+=[MircodSensor.init(sensorName: .INSULIN)]
        case .sensorRESIDUE:
            sensors+=[MircodSensor.init(sensorName: .RESIDUE)]
        case .sensorTEMPERATURE_INSUPEN:
            sensors+=[MircodSensor.init(sensorName: .TEMPERATURE_INSUPEN)]
        case .sensorMOVEMENT:
            sensors+=[MircodSensor.init(sensorName: .MOVEMENT)]
        case .sensorSTEPS:
            sensors+=[MircodSensor.init(sensorName: .STEPS)]
        case .sensorSPEED:
            sensors+=[MircodSensor.init(sensorName: .SPEED)]
        case .sensorECG_PROCESS:
            sensors+=[MircodSensor.init(sensorName: .ECG_PROCESS)]
        default: sensors+=[MircodSensor.init(sensorName: .ERROR)];
            
        }
        return sensors
    }
    
}

