import Foundation
import CoreBluetooth


public protocol MircodScannerDelegate {
    /*!
     * @param peripheral the new found MircodPeripheral
     * @discussion Implement this method to  receive information on the new found devices
     */
    func newPeripheral(_ peripheral :MircodPeripheral)
    /*!
     *  @param state  The new state
     *  @discussion     Invoked whenever the central manager's state has been updated.
     */
    func bluetoothManagerDidUpdateState(_ state : MicodScannerState)
    /*!
     *  @param peripheral  The CBPeripheral that has failed to connect.
     *  @param error   The cause of the failure.
     *  @discussion     This method is invoked when a connection initiated and
     has failed to complete. As connection attempts do not timeout, the failure of a connection is atypical and usually indicative of a transient issue.
     */
    func didFailConnectToPeripheral(_ peripheral : MircodPeripheral ,  withError error : Error?)
    /*!
     *  @param peripheral  The CBPeripheral that has failed to connect.
     *  @discussion     This method is invoked when a connection initiated and has succeeded.
     */
    func didConnectToPeripheral(_ peripheral : MircodPeripheral)
    
    /*!
     *  @param peripheral  The CBPeripheral that has failed to connect.
     *  @param error   The cause of the failure.
     *  @discussion     This method is invoked upon the disconnection of a peripheral that was connected.
     */
    func  disconnectPeripheral(_ peripheral :MircodPeripheral , error : Error?)
}

/*!
 *  @class MircodScanner
 *
 *  @discussion Represents class from mircod company for work with controling MircodPeripheral
 */
open class MircodScanner : NSObject {
    
    private var hiddenDelegate: PrivateMircodScanner?
    
    /*!
     *  @property scannerDelegate
     *  @discussion The delegate object that will receive central events like scaning, connection and other.
     */
    open var scannerDelegate: MircodScannerDelegate?{
        get{
            return self.hiddenDelegate?.scannerDelegate
        }
        set {
            self.hiddenDelegate?.scannerDelegate = newValue
        }
    }
    /*!
     *  @property peripheralDelegate
     *  @discussion The delegate object that will receive central events like resieve data, observe and disobserve to characteristic.
     *
     */
    open var peripheralDelegate: MircodPeripheralDelegate?{
        get{
            return self.hiddenDelegate?.peripheralDelegate
        }
        set {
            self.hiddenDelegate?.peripheralDelegate = newValue
        }
    }
    /*!
     *  @property peripherals
     *  @discussion Mutable array  of the CBPeripheral after scanning.
     *
     */
    open var peripherals: [MircodPeripheral]{
        get{
            return (self.hiddenDelegate?.peripherals)!
        }
    }
    /*!
     *  @property selectedPeripheral is array of MircodPeripheral
     *  @discussion array of MircodPeripheral which selected to connect
     */
    open var selectedPeripherals : [MircodPeripheral]{
        get{
            return (self.hiddenDelegate?.selectedPeripherals)!
        }
    }
    
    /*!
     *  @property manager instants of CBCentralManager
     *  @discussion Whether or not the central is currently scanning.
     */
    open var manager : CBCentralManager?{
        get{
            return self.hiddenDelegate?.manager
        }
    }
    
    public override init() {
        super.init()
    }
    
    /*!
     *  @param delegate The delegate that will receive central role events.
     *  @discussion The initialization call. The events of the central role will be dispatched delegate.
     */
    public convenience init(delegate : MircodScannerDelegate) {
        self.init()
        hiddenDelegate = PrivateMircodScanner.init(delegate: delegate, outer: self)
    }
    
    /*!
     *  @property manager instants of CBCentralManager
     *  @discussion set current central manager
     *
     */
    open func updateManager(manager : CBCentralManager){
        hiddenDelegate?.updateManager(manager: manager)
    }
    /*!
     * Scan peripherals
     */
    open func startScanDevices(_ categoryes: [Category]?) {
        hiddenDelegate?.startScanDevices(categoryes)
    }
    open func startScanDevices(withUUIDs uuids : [CBUUID]) {
        hiddenDelegate?.startScanDevices(withUUIDs: uuids)
    }
    /*!
     * Stop scan peripherals
     */
    open func stopScanDevices() {
        hiddenDelegate?.stopScanDevices()
    }
    /**
     *
     * @param peripheral of instance MircodPeripheral.
     *  @discussion Cancel connect with perpheral
     */
    open func cancelConnect(withPeripheral peripheral : MircodPeripheral){
        hiddenDelegate?.cancelConnect(withPeripheral : peripheral)
    }
    
    /**
     * @param peripheral of instance MircodPeripheral.
     * @param delegate instanse of object which implements protocol MircodPeripheralDelegate
     *  @discussion Connect to perpheral with delegate which will receive the events of the central role.
     */
    open func connect(toPeripheral peripheral: MircodPeripheral, withTimeout timeOut: TimeInterval?, withDelegate delegate: MircodPeripheralDelegate) {
        hiddenDelegate?.connect(toPeripheral: peripheral, withTimeout: timeOut, withDelegate: delegate)
    }
    
    open func timerInvalidate() {
        hiddenDelegate?.timerInvalidate()
    }
    /**
     
     Connect to perpheral with delegate which will receive the events of the central role.
     
     - parameters:
     - identifiers: is array of UUID
     */
    open func retrievePeripherals(withIdentifiers identifiers: [UUID]) -> [MircodPeripheral] {
        return  hiddenDelegate?.retrievePeripherals(withIdentifiers:identifiers) ?? [MircodPeripheral]()
    }
    
    
}

private class PrivateMircodScanner  : NSObject, CBCentralManagerDelegate {
    private weak var outer: MircodScanner?
    open var scannerDelegate: MircodScannerDelegate?
    open var peripheralDelegate: MircodPeripheralDelegate?
    
    
    private var deviceForScan : [CBUUID]?
    private var deviceCategoryesForScan : [Category]?
    
    @available(iOS 9.0, *)
    var isScanning : Bool{
        return self.manager!.isScanning
    }
    
    var manager : CBCentralManager?
    private var timer               = Timer()
    private var selectedPeripheral  = MircodPeripheral()
    
    var peripherals  = [MircodPeripheral]()
    var selectedPeripherals = [MircodPeripheral]()
    
    
    override init() {
        super.init()
        manager = CBCentralManager()
        manager?.delegate = self
    }
    
    
    convenience init(delegate : MircodScannerDelegate , outer : MircodScanner) {
        self.init()
        self.outer = outer
        scannerDelegate = delegate
        peripherals = [MircodPeripheral]()
        selectedPeripherals = [MircodPeripheral]()
    }
    func updateManager(manager : CBCentralManager){
        self.manager = manager
        manager.delegate = self
    }
    
    //MARK - MircodScannerProtocol
    func startScanDevices(_ categoryes : [Category]?) {
        deviceCategoryesForScan = categoryes
        peripherals.removeAll()
        if !selectedPeripherals.isEmpty {
            peripherals.append(contentsOf: selectedPeripherals)
        }
        guard categoryes != nil else {
            manager?.scanForPeripherals(withServices: nil , options: nil)
            print("Start scan devices")
            return
        }
        var uuidArr = [CBUUID]()
        for category in categoryes! {
            switch category {
            case .PatchCategory:
                uuidArr.append(CBUUID(string: PATCH_UUID))
            case .ToolCategory:
                uuidArr.append(CBUUID(string: HUMMER_UUID))
            case .BrushCategory:
                uuidArr.append(CBUUID(string: BRUSH_UUID))
            case .TempPatchCategory:
                uuidArr.append(CBUUID(string: TEMP_PATCH_UUID))
            case .DexcomCategory:
                continue
            case .InsupenCategory:
                uuidArr.append(CBUUID(string: INSUPEN_UUID))
            case .ERRORCategory:
                break
            case .CardioStudyCategory:
                uuidArr.append(CBUUID(string: CARDIOSTUDY_UUID))
            }
        }
        uuidArr.append(CBUUID(string: BOOTLOADER_SERVICES_UUID))
        manager?.scanForPeripherals(withServices: uuidArr, options: nil)
        print("Start scan devices")
    }
    
    func startScanDevices(withUUIDs uuids : [CBUUID]) {
        deviceForScan = uuids
        peripherals.removeAll()
        if !selectedPeripherals.isEmpty {
            peripherals.append(contentsOf: selectedPeripherals)
        }
        if  uuids.isEmpty{
            print("UUIDs array is empty")
        }
        else {
            manager?.scanForPeripherals(withServices: uuids, options: nil)
            print("Start scan devices")
            
        }
    }
    
    func stopScanDevices() {
        manager?.stopScan()
        print("Stop scan devices")
    }
    
    func cancelConnect(withPeripheral peripheral : MircodPeripheral){
        if selectedPeripherals.contains(peripheral) {
            manager?.cancelPeripheralConnection(peripheral.peripheral!)
            selectedPeripherals.remove(object: peripheral)
        }
    }
    
    func connect(toPeripheral peripheral: MircodPeripheral, withTimeout timeOut:TimeInterval?, withDelegate delegate: MircodPeripheralDelegate) {
        peripheralDelegate = delegate
        selectedPeripheral = peripheral
        manager?.connect(peripheral.peripheral!, options: [
            CBConnectPeripheralOptionNotifyOnConnectionKey: true,
            CBConnectPeripheralOptionNotifyOnDisconnectionKey: true,
            CBConnectPeripheralOptionNotifyOnNotificationKey: true
            ])
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: timeOut ?? TimeInterval(exactly: 15)!, target: self, selector: #selector(peripheralDisconnect), userInfo: self, repeats: false)
    }
    
    func timerInvalidate() {
        timer.invalidate()
    }
    
    @objc func peripheralDisconnect() throws{
        manager?.cancelPeripheralConnection(selectedPeripheral.peripheral!)
        print("throw MircodError.timeOut")
        scannerDelegate?.didFailConnectToPeripheral(selectedPeripheral, withError: MircodError.timeOut)
        timer.invalidate()
    }
    
    func retrievePeripherals(withIdentifiers identifiers: [UUID]) -> [MircodPeripheral] {
        var mircodPeripherals = [MircodPeripheral]()
        let retrievePeripherals = manager?.retrievePeripherals(withIdentifiers: identifiers)
        for per in retrievePeripherals! {
            mircodPeripherals.append(MircodPeripheral.init(peripheral: per))
        }
        return mircodPeripherals
    }
    
    //MARK - CBCentralManagerDelegate
    public func centralManagerDidUpdateState(_ central: CBCentralManager){
        switch central.state {
        case .poweredOn :
            scannerDelegate?.bluetoothManagerDidUpdateState(.on)
            peripherals.removeAll()
            if deviceForScan != nil {
                startScanDevices(withUUIDs: deviceForScan!)
            }
            else {
                if deviceCategoryesForScan != nil {
                    startScanDevices(deviceCategoryesForScan)
                } else {
                    startScanDevices(nil)
                }
            }
        case .poweredOff:
            scannerDelegate?.bluetoothManagerDidUpdateState(.off)
            for peripheral in selectedPeripherals{
                cancelConnect(withPeripheral: peripheral)
            }
        case .resetting: scannerDelegate?.bluetoothManagerDidUpdateState(.resetting)
        case .unauthorized:scannerDelegate?.bluetoothManagerDidUpdateState(.unauthorized)
        case .unknown:scannerDelegate?.bluetoothManagerDidUpdateState(.unknown)
        case .unsupported: scannerDelegate?.bluetoothManagerDidUpdateState(.unsupported)
            
        }
        
    }
    
    public func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]){
        print("willRestoreState")
    }
    
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber){
        if peripheral.name != nil{
            var mircodPeripheral : MircodPeripheral?
            
            for  selected in peripherals {
                if selected.peripheral == peripheral {
                    mircodPeripheral = selected;
                    break;
                }
            }
            if mircodPeripheral == nil {
                mircodPeripheral = MircodPeripheral.init(peripheral: peripheral, advertisementData: advertisementData)
                peripherals.append (mircodPeripheral!)
            }
            
            mircodPeripheral?.scanner = self.outer
            scannerDelegate!.newPeripheral(mircodPeripheral!)
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral){
        timer.invalidate()
        var currentMircodPeripheral : MircodPeripheral?
        for peripheralCurr in peripherals {
            if peripheralCurr.peripheral == peripheral{
                peripheralCurr.delegate = peripheralDelegate
                currentMircodPeripheral = peripheralCurr
                break
            }
        }
        if currentMircodPeripheral != nil {
            currentMircodPeripheral?.isConnected = true
            selectedPeripherals.append(currentMircodPeripheral!)
            scannerDelegate?.didConnectToPeripheral(currentMircodPeripheral!)
            peripheral.discoverServices(nil)
        }
    }
    
    
    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?){
        print("didFailToConnect")
        
    }
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?){
        
        print("Did disconnect ", error ?? "no error" )
        
        for peripheralCurr in peripherals {
            if peripheralCurr.peripheral == peripheral{
                peripheralCurr.isConnected = false
                selectedPeripherals.remove(object: peripheralCurr)
                peripheralCurr.clean()
                if error == nil {
                    peripherals.remove(object: peripheralCurr)
                    scannerDelegate?.disconnectPeripheral(peripheralCurr, error: error)
                }
                else {
                    scannerDelegate?.disconnectPeripheral(peripheralCurr, error: nil)
                }
                break;
            }
        }
    }
    
    
}
